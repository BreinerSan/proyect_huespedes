$(document).ready(function(){
    
    ocultarCollapse();

    $('.text-hora').timepicker({
        timeFormat: 'HH:mm:ss',
    });

    function ocultarCollapse(){
        $('.e-collapse').hide();
    }

    function mostrarCollapse(){
        // Menu tabs
        $('.e-collapse').show();
    }

    /**
     * Envia el formulario para crear nuevo huesped
     */
    $('#store').click(function(e){
        e.preventDefault();
        $('#myTab a[href="#tab_1"]').tab('show');
        $('#mostrar_loading').addClass('loader');
    
        $('.error_c').remove();
        
        var data = new FormData($('#form-huesped')[0]);
        var token = $("input[name=_token]").val();
    
        $.ajax({
            url: 'huesped',
            headers: {'X-CSRF-TOKEN':token},
            type: 'POST',
            dataType: 'json',
            processData: false,
            contentType: false,
            data: data,
            success: function(response){
                toastr.success(response.message);
                $("#form-huesped")[0].reset();
    
                $('.list-huesped').append(agregarHuesped(response.huesped));
            },
            error: function(response){
                console.log('log', response);
                if(response.status === 422){
                    $.each(response.responseJSON.errors, function(key, value){
                        var name = $("[name='"+key+"']");
                        if(key.indexOf(".") != -1){
                            var arr = key.split(".");
                            name = $("[name='"+arr[0]+"[]']:eq("+arr[1]+")");
                        }
                        name.parent().append('<span class="error_c" style="color: red;">'+value[0]+'</span>');
                        name.focus();
                    });
                   
                    $('#collapseCantidadesAutorizadas').collapse('show');
                    $('#collapseMallaTurnos').collapse('show');
                    $('#collapseNotificaciones').collapse('show');
                    $('#collapseOne').collapse('show');
                    $('#collapseTwo').collapse('show');
                    toastr.info('Hubo un problema al validar tus datos');

                    var errores = '<ul class="list-unstyled">';
                    $.each(response.responseJSON.errors, function(key, value){
                        errores += '<li>'+value+'</li>';
                    });
                    errores += '</ul>'

                    Swal.fire({
                        icon: 'info',
                        title: 'Hubo un problema al validar tus datos',
                        html: errores                        
                    });
                }else{
                    if(response.responseJSON.message){
                        toastr.error(response.responseJSON.message);
                    }else{
                        toastr.error('Se ha presentado un error al guardar los datos');
                    }                    
                }
            },
            complete:function(){
                $('#mostrar_loading').removeClass('loader'); 
            }
        });
        
    });
    
    /**
     * Actualiza el huesped actual
     */
    $('#update').click(function(e){
        e.preventDefault();
        $('#myTab a[href="#tab_1"]').tab('show');
        $('#mostrar_loading').addClass('loader');
    
        $('.error_c').remove();
        
        var data = new FormData($('#form-huesped')[0]);
        var token = $("input[name=_token]").val();
        var id = $('input#id').val();
    
        data.append('_method', 'PUT');
        
        $.ajax({
            url: 'huesped/'+id,
            headers: {'X-CSRF-TOKEN':token},
            type: 'POST',
            dataType: 'json',
            processData: false,
            contentType: false,
            data: data,
            success: function(response){
                toastr.success(response.message);

                cambiarHuesped(response.huesped);
            },
            error: function(response){
                console.log('log',response);
                if(response.status === 422){
                    $.each(response.responseJSON.errors, function(key, value){
                        var name = $("[name='"+key+"']");
                        if(key.indexOf(".") != -1){
                            var arr = key.split(".");
                            name = $("[name='"+arr[0]+"[]']:eq("+arr[1]+")");
                        }
                        name.parent().append('<span class="error_c" style="color: red;">'+value[0]+'</span>');
                        name.focus();
                    });
                    $('#collapseCantidadesAutorizadas').collapse('show');
                    $('#collapseMallaTurnos').collapse('show');
                    $('#collapseNotificaciones').collapse('show');
                    $('#collapseOne').collapse('show');
                    $('#collapseTwo').collapse('show');
                    toastr.info('Hubo un problema al validar tus datos');

                    var errores = '<ul class="list-unstyled">';
                    $.each(response.responseJSON.errors, function(key, value){
                        errores += '<li>'+value+'</li>';
                    });
                    errores += '</ul>'

                    Swal.fire({
                        icon: 'info',
                        title: 'Hubo un problema al validar tus datos',
                        html: errores                        
                    });
                }else{
                    toastr.error('Se ha presentado un error al guardar los datos');
                }
                
            },
            complete:function(){
                $('#mostrar_loading').removeClass('loader'); 
            }
        });
        
    });

    /**
     * Busca un huesped en especifico
     */
    $('#search-huesped').on('keyup', '#texto', function(e){
        e.preventDefault();
        let texto = $(this).val();
        
        $.ajax({
            type: 'GET',
            url: 'huesped/search',
            data: {'texto': texto},
            success: function (response) {                
                let html = "";

                $.each(response.huespedes, function(key, value){
                    html += agregarHuesped(value);
                });

                $('.list-huesped').html(html);
            },
            error: function(response) { 
                console.log(response);
            }
        })
    });

    /**
     * Retorna lista de huespedes en html
     */
    function agregarHuesped(huesped){
        let html = `
            <a href="#" class="list-group-item item-huesped">
                <input type="text" id="huesped_id" style="display: none" value="${huesped.id}">
                <h4 class="list-group-item-heading"><b>${huesped.nombre}</b></h4>
                <p class="list-group-item-text text-muted">Teléfono ${huesped.telefono1}</p>
            </a>
            `;
        return html;
    }

    /**
     * Evento que muestra la informacion del Huesped seleccionado
     */
    $('.list-huesped').on('click', '.item-huesped', function(e){
        e.preventDefault();
        $('#mostrar_loading').addClass('loader');
        $('.error_c').remove();

        var id_huesped = $(this).children("input#huesped_id").val();

        $.ajax({
            type: 'GET',
            url: 'huesped/'+id_huesped,
            success: function (response) {
                
                $('h3.title').text(response.huesped.nombre);
                
                $('#form-huesped')[0].reset();
                $('#update').show();
                $('#store').hide();
                $('.data-usuari').hide();

                mostrarCollapse();
                cambiarHuesped(response.huesped);

                getCuentasCorreo(response.huesped.id);
                getProveedoresSms(response.huesped.id);
                listarFestivos(response.huesped.id);
                listarPausas(response.huesped.id);
            },
            error: function(response) { 
                 toastr.error(response.message);
            },
            complete:function(){
                $('#mostrar_loading').removeClass('loader'); 
            }
        })
    });

    /**
     * Sobrescribe los datos del huésped en #form-huesped
     */
    function cambiarHuesped(response){
        let h = response;
        let huesped = $('#form-huesped');

        huesped.find('input#id').val(h.id);
        huesped.find('input#nombre').val(h.nombre);
        huesped.find('input#razon_social').val(h.razon_social);
        huesped.find('input#nit').val(h.nit);
        huesped.find('input#direccion').val(h.direccion);
        huesped.find('input#telefono1').val(h.telefono1);
        huesped.find('input#telefono2').val(h.telefono2);
        huesped.find('input#telefono3').val(h.telefono3);
        huesped.find('input#fotoUsuarioObligatoria').prop('checked', (h.foto_usuario_obligatoria == 1) ? true : false);

        $('#collapseNotificaciones input#notificar_pausas').prop('checked', (h.notificar_pausas == 1) ? true : false);
        $('#collapseNotificaciones input#notificar_sesiones').prop('checked', (h.notificar_sesiones == 1) ? true : false);
        $('#collapseNotificaciones input#notificar_incumplimientos').prop('checked', (h.notificar_incumplimientos == 1) ? true : false);
        $('#collapseNotificaciones input#emails_notificar_pausas').val(h.emails_notificar_pausas);
        $('#collapseNotificaciones input#emails_notificar_sesiones').val(h.emails_notificar_sesiones);
        $('#collapseNotificaciones input#emails_notificar_incumplimientos').val(h.emails_notificar_incumplimientos);

        $('collapseMallaTurnos input#mallaTurnoRequerida').prop('checked', (h.malla_turno_requerida == 1) ? true : false);
        $('collapseMallaTurnos input#mallaTurnoHorarioDefecto').prop('checked', (h.malla_turno_horario_por_defecto == 1) ? true : false);
        $('collapseMallaTurnos input#horaEntrada').val(h.hora_entrada_por_defecto);
        $('collapseMallaTurnos input#horaSalida').val(h.hora_salida_por_defecto);
        if(h.proyecto){
            $('#collapseCantidadesAutorizadas input#cantidadMaxAgentesSimultaneos').val(h.proyecto.cantidad_max_agentes_simultaneos);
        }
        $('#collapseCantidadesAutorizadas input#cantidadMaximaSupervisores').val(h.cantidad_max_supervisores);
        $('#collapseCantidadesAutorizadas input#cantidadMaximaBackoffice').val(h.cantidad_max_bo);

        
        
        if(h.mail_notificacion){
            $('#collapseNotificaciones input#notificacion_servidor_smtp').val(h.mail_notificacion.servidor_smtp);
            $('#collapseNotificaciones input#notificacion_dominio').val(h.mail_notificacion.dominio);
            $('#collapseNotificaciones input#notificacion_puerto').val(h.mail_notificacion.puerto);
            $('#collapseNotificaciones input#notificacion_usuario').val(h.mail_notificacion.usuario);
            $('#collapseNotificaciones input#notificacion_password').val(h.mail_notificacion.password);
            $('#collapseNotificaciones input#notificacion_auth').prop('checked', (h.mail_notificacion.auth == 1) ? true : false);
            $('#collapseNotificaciones input#notificacion_ttls').prop('checked', (h.mail_notificacion.ttls == 1) ? true : false);
            // Agrega el id de mail_notificacion al data-id del boton de test
            $('#btn-test-notificacion-cuenta').data('id', h.mail_notificacion.id);
        }
        $('#btn-test-notificacion-cuenta').show();

        $('#collapseMensajeActual #mensajeActual').val(h.mensaje);

        $.get('pais/ciudad/'+h.id_pais_ciudad+'', function(response){
            
            huesped.find('select#pais').val(response.pais);
            
            $.when(getAllCiudades(response.pais)).then(function(){
                huesped.find('select#ciudad').val(h.id_pais_ciudad);    
            });
        });

        let html_contacto = '';

        $.each(h.contactos, function(key, value){
            html_contacto += generateContacto(value);
        });
        $('#panel-contacto table tbody').html(html_contacto);

        let huesped_archivos = $('#panel-archivos');

        huesped_archivos.find('a.camara_comercio').attr('href', 'huesped/file/'+h.id+'/camara_comercio.pdf');
        huesped_archivos.find('a.rut').attr('href', 'huesped/file/'+h.id+'/rut.pdf');
        huesped_archivos.find('a.certificacion_bancaria').attr('href', 'huesped/file/'+h.id+'/certificacion_bancaria.pdf');
        huesped_archivos.find('a.orden_compra').attr('href', 'huesped/file/'+h.id+'/orden_compra.pdf');
        huesped_archivos.find('a.alcances').attr('href', 'huesped/file/'+h.id+'/alcances.pdf');

        var html_troncales = '';

        if(h.troncales.length > 0){
            $.each(h.troncales, function(key, value){
                html_troncales += getHtmlTroncal(value);
            });
        }else{
            html_troncales =`
                <tr>
                    <td colspan="5"><h4>El huésped aún no ha creado troncales</h4></td>
                </tr>
            `;
        }
        $('#collapseTroncales table tbody').html(html_troncales);

        // Obtiene el estado de las troncales
        getEstadosTroncales(h.id);

        getUsuarios(h.id);
    }

    /**
     * Muestra el formulario para crear huesped
     */
    $('#create').on('click', function(){
        
        $('#form-huesped')[0].reset();
        $('.error_c').remove();

        $('h3.title').text('Registrar huésped');

        $('#panel-contacto table tbody').html(generateContacto());

        $('#store').show();
        $('#update').hide();
        $('.data-usuari').show();

        $('#panel-archivos a.doc').attr('href', '#');

        // Agrega el id por defecto de mail_notificacion al data-id del boton de test
        $('#btn-test-notificacion-cuenta').data('id', -1);
        $('#btn-test-notificacion-cuenta').hide();

        ocultarCollapse();
               
    });

    /**
     * Agrega nuevo campo para registrar nuevo contacto
     */
    $("#add-contacto").click(function(){
        $('#panel-contacto table tbody').append(generateContacto());
    });

    $("#panel-contacto").on('click', '.eliminarContacto', function(){
        $(this).closest('tr').remove();
    });

    /**
     * Crear por defecto objeto contacto vacio
     */
    function Contacto(){
        this.id = -1;
        this.nombre = '';
        this.email = '';
        this.tipo = '';
        this.telefono1 = '';
        this.telefono2 = '';
    }
    
    /**
     * Genera Codigo html para añadir nuevo contacto
     */
    function generateContacto(contacto = new Contacto){
        let html = `
        <tr class="linea-contacto">
            <input type="hidden" name="contacto_id[]" value="${contacto.id}" form="form-huesped">
            <td>
                <input type="text" class="form-control" name="contacto_nombre[]" placeholder="Ingresa el nombre" value="${contacto.nombre}" form="form-huesped">
            </td>
            <td>
                <input type="email" class="form-control" name="contacto_email[]" placeholder="Ingresa el email" value="${contacto.email}" form="form-huesped">
            </td>
            <td>
                <select name="contacto_tipo[]" class="form-control" form="form-huesped">
                    <option value="T" ${contacto.tipo == "T" ? 'selected' : ''}>Tecnico</option>
                    <option value="P" ${contacto.tipo == "P" ? 'selected' : ''}>Pagos</option>
                    <option value="F" ${contacto.tipo == "F" ? 'selected' : ''}>Funcional</option>
                </select>
            </td>
            <td>
                <input type="tel" class="form-control" name="contacto_telefono1[]" placeholder="Ingresa el numero de teléfono" value="${contacto.telefono1}" form="form-huesped">
            </td>
            <td>
                <input type="tel" class="form-control" name="contacto_telefono2[]" placeholder="Ingresa el numero de teléfono" value="${contacto.telefono2}" form="form-huesped">
            </td>
            <td>
                <button type="button" class="btn btn-danger eliminarContacto"><i class="fa fa-trash"></i></button>
            </td>
        </tr>
        `;

        return html;
    }

    /**
     * Devuelve todas las ciudades del pais seleccionado
    */
   $('#form-huesped').on('change', '#pais', function(event){
        getAllCiudades(event.target.value);
    });

    /**
     * Busca todos las ciudades de un pais
     */
    function getAllCiudades(pais){
        var d1 = $.Deferred();
        $.get('pais/ciudades/'+pais+'', function(response, ciudad){
            
            $('#ciudad').empty();
            $('#ciudad').append("<option value=''>Seleccionar</option>");
            for(i=0; i<response.length; i++){
                $('#ciudad').append("<option value='"+response[i].id+"'>"+response[i].ciudad+"</option>");
            }
            d1.resolve();
        });
        return d1.promise();    
    }

    /** Ejecuta el test de cuenta de notificaciones smtp */
    $('#btn-test-notificacion-cuenta').on('click', function(){
        var idCuentaNotificacion = $(this).data('id');
        $('#modal-prueba-notificacionCuenta #notificacionLog').html('');

        $('#modal-prueba-notificacionCuenta .estado-test-notificacion').hide();

        if(idCuentaNotificacion == -1){
            alert('No se puede realizar la prueba hasta no haber registrado el huésped');
        }else{

            $('#modal-prueba-notificacionCuenta').modal('show');

            $('#mostrar_loading').addClass('loader');

            var token = $("input[name=_token]").val();
            var id_huesped = $('input#id').val();
            var f = new Date();
            var dia = f.getDate() +'-'+ (f.getMonth() + 1) +'-'+ f.getFullYear();
            var hora = f.getHours() +':'+ f.getMinutes() +':'+ f.getSeconds();

            $('#modal-prueba-notificacionCuenta #notificacionLog').append(dia +' '+hora+' iniciado &#13;&#10;');
            
            $.ajax({
                url: 'huesped/prueba-email-notificacion-smtp/'+id_huesped,
                headers: {'X-CSRF-TOKEN':token},
                type: 'POST',
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function(response){
                    
                    $('#modal-prueba-notificacionCuenta .estado-test-notificacion').text('Prueba fallida').removeClass('text-success').addClass('text-danger').show();
                    if(response.json){

                        if(response.json.strEstado_t == 'ok'){
                            $('#modal-prueba-notificacionCuenta #notificacionLog').append(dia +' '+hora+' Estado: '+ response.json.strEstado_t +' &#13;&#10;');
                            $('#modal-prueba-notificacionCuenta #notificacionLog').append(dia +' '+hora+' Se ha ejecutado la prueba exitosamente &#13;&#10;');
                            $('#modal-prueba-notificacionCuenta #notificacionLog').append(dia +' '+hora+' Prueba finalizada &#13;&#10;');

                            $('#modal-prueba-notificacionCuenta .estado-test-notificacion').text('Prueba exitosa').removeClass('text-danger').addClass('text-success');
                        }else{
                            hora = f.getHours() +':'+ f.getMinutes() +':'+ f.getSeconds();
                            $('#modal-prueba-notificacionCuenta #notificacionLog').append(dia +' '+hora+' Estado: '+response.json.strEstado_t+' &#13;&#10;');
                            $('#modal-prueba-notificacionCuenta #notificacionLog').append(dia +' '+hora+' Hubo un error al ejecutar la prueba de entrada de correo &#13;&#10;');
                        }

                    }else{
                        $('#modal-prueba-notificacionCuenta #notificacionLog').append(dia +' '+hora+' Ha habido un problema al comunicarse con la api &#13;&#10;');
                    }

                },
                error: function(response){
                    if(response.responseJSON.message){
                        toastr.error(response.responseJSON.message);
                    }else{
                        toastr.error('Se ha presentado un error al consumir la api.');
                    }                    
                },
                complete:function(){
                    $('#mostrar_loading').removeClass('loader'); 
                }
            });
        }
    });

    /**--------------Seccion pausas------------- */
    function listarPausas(id_huesped){
        $.ajax({
            type: 'GET',
            url: 'huesped/listar-pausas/'+id_huesped,
            success: function (response) {
                
                let html11 = '';
                if(response.listaPausas){  
                    $.each(response.listaPausas, function(key, value){
                        html11 += agregarPausas(value);
                    });
                }else{
                    html11 = `
                    <tr>
                        <td colspan="8"><h4>El huésped aún no tiene pausas asignadas</h4></td>
                    </tr>
                    `;
                }
                
                $('#collapsePausas table tbody').html(html11);     
                $('input[name="hora_inicial[]"]').timepicker({
                    timeFormat: 'HH:mm:ss',
                });
                $('input[name="hora_final[]"]').timepicker({
                    timeFormat: 'HH:mm:ss',
                }); 
            },
            error: function(response) { 
                 toastr.error("No se pudo obtener la lista de pausas");
            }
        })
    }


    /**Esta funcion retorna las pausas que tiene el huesped */
    function agregarPausas(data){
        let html = `
        <tr>
            <input type="hidden" name="id_pausa[]" value="${data.id}">
            <td><input type="text" style="width:200px;" name="nombre_pausa[]" class="form-control" value="${data.tipo}"></td>
            <td>
                <select name="clasificacion[]" class="form-control">
                    <option value="">Seleccionar</option>    
                    <option value="0" ${data.descanso == "0" ? 'selected' : ''}>Laboral</option>
                    <option value="1" ${data.descanso == "1" ? 'selected' : ''}>Descanso</option>
                </select>
            </td>
            <td>
                <select name="tipo[]" class="form-control tipo-pausa">
                    <option value="">Seleccionar</option>    
                    <option value="0" ${data.tipo_pausa == "0" ? 'selected' : ''}>Sin horario exacto</option>
                    <option value="1" ${data.tipo_pausa == "1" ? 'selected' : ''}>Con horario exacto</option>
                </select>
            </td>
            <td><input type="text" name="hora_inicial[]" class="form-control" value="${data.hora_inicial_por_defecto ? data.hora_inicial_por_defecto : ''}" ${data.tipo_pausa == "0" ? 'readonly' : ''}></td>
            <td><input type="text" name="hora_final[]" class="form-control" value="${data.hora_final_por_defecto ? data.hora_final_por_defecto : ''}" ${data.tipo_pausa == "0" ? 'readonly' : ''}></td>
            <td><input type="number" name="duracion_maxima[]" class="form-control" value="${data.duracion_maxima ? data.duracion_maxima : ''}" ${data.tipo_pausa == "1" ? 'readonly' : ''}></td>
            <td><input type="number" name="cantidad_maxima[]" class="form-control" value="${data.cantidad_maxima_evento_dia ? data.cantidad_maxima_evento_dia : ''}" ${data.tipo_pausa == "1" ? 'readonly' : ''}></td>
            <td>
                <button type="button" data-id="${data.id}" class="btn btn-danger btn-eliminar-pausa"><i class="fa fa-trash"></i></button>
            </td>
        </tr>
        `;
        return html;
    }

    /** Agregar nueva pausa a la tabla */
    $('#btn-agregar-pausa').on('click', function(){
        let html = `
        <tr>
            <input type="hidden" name="id_pausa[]" value="-1">
            <td><input type="text" name="nombre_pausa[]" class="form-control"></td>
            <td>
                <select name="clasificacion[]" class="form-control">
                    <option value="">Seleccionar</option>    
                    <option value="0">Laboral</option>
                    <option value="1">Descanso</option>
                </select>
            </td>
            <td>
                <select name="tipo[]" class="form-control tipo-pausa">
                    <option value="">Seleccionar</option>    
                    <option value="0">Sin horario exacto</option>
                    <option value="1">Con horario exacto</option>
                </select>
            </td>
            <td><input type="text" name="hora_inicial[]" class="form-control"></td>
            <td><input type="text" name="hora_final[]" class="form-control"></td>
            <td><input type="text" name="duracion_maxima[]" class="form-control"></td>
            <td><input type="text" name="cantidad_maxima[]" class="form-control"></td>
            <td>
                <button type="button" data-id="-1" class="btn btn-danger btn-eliminar-pausa"><i class="fa fa-trash"></i></button>
            </td>
        </tr>
        `;

        $('#collapsePausas table tbody').append(html);
        $('input[name="hora_inicial[]"]').timepicker({
            timeFormat: 'HH:mm:ss',
        });
        $('input[name="hora_final[]"]').timepicker({
            timeFormat: 'HH:mm:ss',
        }); 
    });

    /** cambiar si es readonly dependiendo del tipo de pausa */
    $('#collapsePausas table').on('change', '.tipo-pausa', function(){
        var idTipoPausa = $(this).val();
        var trPadre = $(this).closest('tr');
        
        if(idTipoPausa == 0){
            trPadre.find("[name='hora_inicial[]']").prop('readonly','true');
            trPadre.find("[name='hora_final[]']").prop('readonly','true');
            trPadre.find("[name='duracion_maxima[]']").prop('readonly','');
            trPadre.find("[name='cantidad_maxima[]']").prop('readonly','');
        }else if(idTipoPausa == 1){
            trPadre.find("[name='hora_inicial[]']").prop('readonly','');
            trPadre.find("[name='hora_final[]']").prop('readonly','');
            trPadre.find("[name='duracion_maxima[]']").prop('readonly','true');
            trPadre.find("[name='cantidad_maxima[]']").prop('readonly','true');
        }
        
    });

    /** Eliminar pausa */
    $('#collapsePausas table').on('click', '.btn-eliminar-pausa', function(){
        $(this).closest('tr').remove();
    });

    /** Guardar Cambios en la tabla de pausas */
    $('#btn-guardar-cambios-pausas').on('click', function(e){
        e.preventDefault();

        $('#mostrar_loading').addClass('loader');
        $('#form-pausas .error_c').remove();
        
        var data = new FormData($('#form-pausas')[0]);
        var token = $("input[name=_token]").val();
        var id_huesped = $('input#id').val();

        $.ajax({
            url: 'huesped/guardar-pausas/'+id_huesped,
            headers: {'X-CSRF-TOKEN':token},
            type: 'POST',
            dataType: 'json',
            processData: false,
            contentType: false,
            data: data,
            success: function(response){
                toastr.success(response.message);
                listarPausas(id_huesped);
            },
            error: function(response){
                if(response.status === 422){
                    $.each(response.responseJSON.errors, function(key, value){
                        var name = $("#form-pausas [name='"+key+"']");
                        if(key.indexOf(".") != -1){
                            var arr = key.split(".");
                            name = $("#form-pausas [name='"+arr[0]+"[]']:eq("+arr[1]+")");
                        }
                        name.parent().append('<span class="error_c" style="color: red;">'+value[0]+'</span>');
                        name.focus();
                    });
                    toastr.info('Hubo un problema al validar tus datos');
                }else{
                    if(response.responseJSON.message){
                        toastr.error(response.responseJSON.message);
                    }else{
                        toastr.error('Se ha presentado un error al guardar los datos.');
                    }                    
                }
            },
            complete:function(){
                $('#mostrar_loading').removeClass('loader'); 
            }
        });
        
    });


    /*------------- Seccion Festivos -------------*/

    /** Listar fechas */
    function listarFestivos(id_huesped){

        $.ajax({
            type: 'GET',
            url: 'huesped/listar-festivos/'+id_huesped,
            success: function (response) {
                
                let html11 = '';
                if(response.listaFestivos && response.listaFestivos.festivos.length > 0){  
                    $('#collapseFestivos .text-festivo').text(response.listaFestivos.nombre);
                    $.each(response.listaFestivos.festivos, function(key, value){
                        html11 += agregarFestivos(value, key);
                    });
                }else{
                    $('#collapseFestivos .text-festivo').text('');
                    html11 = `
                    <tr>
                        <td colspan="5"><h4>El huésped aún no tiene asignada fechas de festivos</h4></td>
                    </tr>
                    `;
                }
                
                $('#collapseFestivos table tbody').html(html11);      
                
                // Agrega las propiedades de .datepicker al campo
                inicializarDatePicker();
            },
            error: function(response) { 
                 toastr.error("No se pudo obtener la lista de festivos");
            }
        })
    }

    function agregarFestivos(data, key){
        let html = `
        <tr>
            <input type="hidden" name="festivo_id[]" value="${data.id}">
            <td>${key+1}</td>
            <td><input type="text" name="nombre_festivo[]" class="form-control" value="${data.nombre}"></td>
            <td><input type="text" name="fecha_festivo[]" class="form-control datepicker" value="${data.fecha}"></td>
            <td>
                <button type="button" data-id="${data.id}" class="btn btn-danger btn-eliminar-fecha"><i class="fa fa-trash"></i></button>
            </td>
        </tr>
        `;
        return html;
    }

    /** Agregar nueva fecha */
    $('#btn-agregar-fecha').on('click', function(){
        let html = `
        <tr>
            <input type="hidden" name="festivo_id[]" value="-1">
            <td></td>
            <td><input type="text" name="nombre_festivo[]" class="form-control"></td>
            <td><input type="text" name="fecha_festivo[]" class="form-control datepicker"></td>
            <td>
                <button type="button" data-id="-1" class="btn btn-danger btn-eliminar-fecha"><i class="fa fa-trash"></i></button>
            </td>
        </tr>
        `;

        $('#collapseFestivos table tbody').append(html);
        inicializarDatePicker();
    });

    /** Eliminar fecha */
    $('#collapseFestivos table').on('click', '.btn-eliminar-fecha', function(){
        
        // let id_festivo = $(this).data('id');

        // if(id_festivo < 0){
        //     $(this).closest('tr').remove();
        // }
        $(this).closest('tr').remove();
        
    });

    /** Guardar Cambios fecha */
    $('#btn-guardar-cambios-fecha').on('click', function(e){
        e.preventDefault();

        $('#mostrar_loading').addClass('loader');
        $('#form-festivos .error_c').remove();

        var data = new FormData($('#form-festivos')[0]);
        var token = $("input[name=_token]").val();
        var id_huesped = $('input#id').val();

        $.ajax({
            url: 'huesped/guardar-festivos/'+id_huesped,
            headers: {'X-CSRF-TOKEN':token},
            type: 'POST',
            dataType: 'json',
            processData: false,
            contentType: false,
            data: data,
            success: function(response){
                toastr.success(response.message);
                listarFestivos(id_huesped);
            },
            error: function(response){
                if(response.status === 422){
                    $.each(response.responseJSON.errors, function(key, value){
                        var name = $("#form-festivos [name='"+key+"']");
                        if(key.indexOf(".") != -1){
                            var arr = key.split(".");
                            name = $("#form-festivos [name='"+arr[0]+"[]']:eq("+arr[1]+")");
                        }
                        name.parent().append('<span class="error_c" style="color: red;">'+value[0]+'</span>');
                        name.focus();
                    });
                    toastr.info('Hubo un problema al validar tus datos');
                }else{
                    if(response.responseJSON.message){
                        toastr.error(response.responseJSON.message);
                    }else{
                        toastr.error('Se ha presentado un error al guardar los datos.');
                    }                    
                }
            },
            complete:function(){
                $('#mostrar_loading').removeClass('loader'); 
            }
        });
        
    });

    function inicializarDatePicker(){
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });
    }

    /*------------- Proveedores de sms -------------*/

    /** Evento button que muestra la modal para crear un nuevo registro */
    $('#btn-crear-proveedor-sms').on('click', function(){
        $("#form-proveedor-sms")[0].reset();
        $('#modal-proveedorSms .modal-title').text("Registrar proveedor SMS");
        $('#form-proveedor-sms .error_c').remove();
        $("#form-proveedor-sms #accion").val('registrar');
        $('#modal-proveedorSms').modal('show');
    });

    /**
     * Evento button que muestra el modal para editar
     */
    $('#collapseProveedoresSms table').on('click', '.btn-editar-proveedor-sms', function(){

        $("#form-proveedor-sms")[0].reset();
        $('#modal-proveedorSms .modal-title').text("Editar proveedor SMS");
        $("#form-proveedor-sms #accion").val('actualizar');
        $('#modal-proveedorSms').modal('show');
        
        $('#form-proveedor-sms .error_c').remove();
        $('#mostrar_loading').addClass('loader');

        var id = $(this).data('id');

        $.ajax({
            type: 'GET',
            url: 'huesped/proveedor-sms/'+id,
            success: function (response) {           
                editarProveedorSms(response.proveedorSms);
            },
            error: function(response) { 
                 toastr.error(response.message);
            },
            complete:function(){
                $('#mostrar_loading').removeClass('loader'); 
            }
        })
    });

    /** Guarda el registro del formulario store y update */
    $('#modal-proveedorSms #btn-guardar-proveedor-sms').on('click', function(e){
        e.preventDefault();
        $('#mostrar_loading').addClass('loader');
        $('#form-proveedor-sms .error_c').remove();
        var data = new FormData($('#form-proveedor-sms')[0]);
        var token = $("input[name=_token]").val();
        var id_huesped = $('input#id').val();
        var accion = $('#form-proveedor-sms #accion').val();

        switch (accion) {
            case 'registrar':
                $.ajax({
                    url: 'huesped/crear-proveedor-sms/'+id_huesped,
                    headers: {'X-CSRF-TOKEN':token},
                    type: 'POST',
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data: data,
                    success: function(response){
                        
                        toastr.success(response.message);
                        $("#form-proveedor-sms")[0].reset();
                        getProveedoresSms(id_huesped);
                        $('#modal-proveedorSms').modal('hide');
                    },
                    error: function(response){
                        if(response.status === 422){
                            $.each(response.responseJSON.errors, function(key, value){
                                var name = $("#form-proveedor-sms [name='"+key+"']");
                                if(key.indexOf(".") != -1){
                                    var arr = key.split(".");
                                    name = $("#form-proveedor-sms [name='"+arr[0]+"[]']:eq("+arr[1]+")");
                                }
                                name.parent().append('<span class="error_c" style="color: red;">'+value[0]+'</span>');
                                name.focus();
                            });
                            toastr.info('Hubo un problema al validar tus datos');
                        }else{
                            if(response.responseJSON.message){
                                toastr.error(response.responseJSON.message);
                            }else{
                                toastr.error('Se ha presentado un error al guardar los datos.');
                            }                    
                        }
                    },
                    complete:function(){
                        $('#mostrar_loading').removeClass('loader'); 
                    }
                });
                break;
        
            case 'actualizar':
                data.append('_method', 'PUT');
                var id = $('#form-proveedor-sms #id_proveedorSms').val();
                $.ajax({
                    url: 'huesped/actualizar-proveedor-sms/'+id,
                    headers: {'X-CSRF-TOKEN':token},
                    type: 'POST',
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data: data,
                    success: function(response){
                        toastr.success(response.message);
                        getProveedoresSms(response.proveedorSms.id_huesped);
                        $('#modal-proveedorSms').modal('hide');
                    },
                    error: function(response){
                        if(response.status === 422){
                            $.each(response.responseJSON.errors, function(key, value){
                                var name = $("#form-proveedor-sms [name='"+key+"']");
                                if(key.indexOf(".") != -1){
                                    var arr = key.split(".");
                                    name = $("#form-proveedor-sms [name='"+arr[0]+"[]']:eq("+arr[1]+")");
                                }
                                name.parent().append('<span class="error_c" style="color: red;">'+value[0]+'</span>');
                                name.focus();
                            });
                            toastr.info('Hubo un problema al validar tus datos');
                        }else{
                            if(response.responseJSON.message){
                                toastr.error(response.responseJSON.message);
                            }else{
                                toastr.error('Se ha presentado un error al guardar los datos.');
                            }                    
                        }
                    },
                    complete:function(){
                        $('#mostrar_loading').removeClass('loader'); 
                    }
                });
                break;
            default:
                break;
        }
    });

    /** Elimina el registro seleccionado */
    $('#collapseProveedoresSms table').on('click', '.btn-eliminar-proveedor-sms', function(){
        var id = $(this).data('id');
        var id_huesped = $('input#id').val();
        var token = $("input[name=_token]").val();

        Swal.fire({
            title: 'Deseas eliminar este proveedor de SMS?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminar!'
        }).then((result) => {
            if (result.value) {
                $('#mostrar_loading').addClass('loader');
                $.ajax({
                    headers: {'X-CSRF-TOKEN':token},
                    type: 'DELETE',
                    url: 'huesped/eliminar-proveedor-sms/'+id,
                    data: {
                        "id": id
                    },
                    success: function (response) {           
                        getProveedoresSms(id_huesped);
                        Swal.fire(
                            'Eliminado!',
                            'Su registro ha sido eliminado.',
                            'success'
                        )
                    },
                    error: function(response) { 
                         toastr.error(response.responseJSON.message);
                    },
                    complete:function(){
                        $('#mostrar_loading').removeClass('loader'); 
                    }
                })
              
            }
        })
    });

    /** Inserta los datos de proveedor Sms al formulario de editar */
    function editarProveedorSms(data){
        let cuentaCorreo = $('#form-proveedor-sms');

        cuentaCorreo.find('input#id_proveedorSms').val(data.id);
        cuentaCorreo.find('input#nombre').val(data.nombre);
        cuentaCorreo.find('select#proveedor').val(data.proveedor);
        cuentaCorreo.find('input#url_api').val(data.url_api);
        cuentaCorreo.find('input#url_api_ssl').val(data.url_api_ssl);
        cuentaCorreo.find('input#api_key').val(data.api_key);

    }

    /**Busca todos las cuentas de correo perteneciente a un huesped */
    function getProveedoresSms(id){

        $.ajax({
            type: 'GET',
            url: 'huesped/listar-proveedores-sms/'+id,
            success: function (response) {
                let html11 = '';
                if(response.proveedoresSms.length > 0){    
                    $.each(response.proveedoresSms, function(key, value){
                        html11 += agregarProveedorSms(value, key);
                    });
                }else{
                    html11 = `
                    <tr>
                        <td colspan="5"><h4>El huésped aún no tiene proveedores sms</h4></td>
                    </tr>
                    `;
                }
                
                 $('#collapseProveedoresSms table tbody').html(html11);      
            },
            error: function(response) { 
                 toastr.error("No se pudo obtener los proveedores SMS");
            }
        })
    };

    /** Agrega una fila a la tabla de proveedores de sms */
    function agregarProveedorSms(data, key){
        let html = `
        <tr>
            <td>${key+1}</td>
            <td>${data.nombre}</td>
            <td>${data.proveedor}</td>
            <td>
                <button data-id="${data.id}" class="btn btn-primary btn-sm btn-editar-proveedor-sms" title="Editar">
                    <i class="fa fa-edit"></i>
                </button>
                <button data-id="${data.id}" class="btn btn-danger btn-sm btn-eliminar-proveedor-sms" title="Eliminar">
                    <i class="fa fa-trash"></i>
                </button>
                <button class="btn btn-info btn-sm btn-prueba-sms" data-id="${data.id}" data-huesped="${data.id_huesped}" data-nombre="${data.nombre}">Prueba</button>
            </td>
        </tr>
        `;
        
        return html;
    }

    /** Abre la modal para la prueba de sms */
    $('#collapseProveedoresSms table').on('click', '.btn-prueba-sms', function(){
        
        $("#form-prueba-proveedor-sms")[0].reset();
        $('#modal-prueba-proveedorSms').modal('show');

        $('#modal-prueba-proveedorSms .estado-test-sms').hide();
        
        $('#form-prueba-proveedor-sms .error_c').remove();

        var id = $(this).data('id');
        var id_huesped = $(this).data('huesped');
        var nombre = $(this).data('nombre');

        $('#form-prueba-proveedor-sms #id_proveedorSms').val(id);
        $('#form-prueba-proveedor-sms #id_huesped').val(id_huesped);
        $('#form-prueba-proveedor-sms .nombre-proveedor-sms').text(nombre);

        $('#modal-prueba-proveedorSms #consoleLog').hide();
    });

    /** Ejecuta la prueba de sms */
    $('#modal-prueba-proveedorSms #btn-probar-proveedor-sms').on('click', function(){
        $('#mostrar_loading').addClass('loader');
        $('#form-prueba-proveedor-sms .error_c').remove();
        var data = new FormData($('#form-prueba-proveedor-sms')[0]);
        var token = $("input[name=_token]").val();

        $('#modal-prueba-proveedorSms #consoleLog').html('');
        $('#modal-prueba-proveedorSms #consoleLog').show();

        var f = new Date();

        var dia = f.getDate() +'-'+ (f.getMonth() + 1) +'-'+ f.getFullYear();
        var hora = f.getHours() +':'+ f.getMinutes() +':'+ f.getSeconds();

        $('#modal-prueba-proveedorSms #consoleLog').append('Test de proveedor de SMS &#13;&#10;');
        $('#modal-prueba-proveedorSms #consoleLog').append(dia +' '+hora+' Test iniciado &#13;&#10;');        
        
        $.ajax({
            url: 'huesped/prueba-proveedor-sms',
            headers: {'X-CSRF-TOKEN':token},
            type: 'POST',
            dataType: 'json',
            processData: false,
            contentType: false,
            data: data,
            success: function(response){

                $('#modal-prueba-proveedorSms .estado-test-sms').text('Prueba fallida').removeClass('text-success').addClass('text-danger').show();

                if(response.json){
                    if(response.json.strEstado_t == 'ok'){
                        hora = f.getHours() +':'+ f.getMinutes() +':'+ f.getSeconds();
                        $('#modal-prueba-proveedorSms #consoleLog').append(dia +' '+hora+' Estado '+response.json.strEstado_t+' &#13;&#10;');
                        $('#modal-prueba-proveedorSms #consoleLog').append(dia +' '+hora+' Mensaje '+response.message+' &#13;&#10;');
                        $('#modal-prueba-proveedorSms #consoleLog').append(dia +' '+hora+' Test finalizado &#13;&#10;');

                        $('#modal-prueba-proveedorSms .estado-test-sms').text('Prueba exitosa').removeClass('text-danger').addClass('text-success');
                    }else{
                        hora = f.getHours() +':'+ f.getMinutes() +':'+ f.getSeconds();
                        $('#modal-prueba-proveedorSms #consoleLog').append(dia +' '+hora+' Estado '+response.json.strEstado_t+' &#13;&#10;');
                        $('#modal-prueba-proveedorSms #consoleLog').append(dia +' '+hora+' Mensaje '+response.json.strMensaje_t+' &#13;&#10;');
                        $('#modal-prueba-proveedorSms #consoleLog').append(dia +' '+hora+' Test finalizado &#13;&#10;');
                    }
                }else{
                    hora = f.getHours() +':'+ f.getMinutes() +':'+ f.getSeconds();
                    $('#modal-prueba-proveedorSms #consoleLog').append(dia +' '+hora+' Ha habido un problema al comunicarse con la api &#13;&#10;');
                    $('#modal-prueba-proveedorSms #consoleLog').append(dia +' '+hora+' Test finalizado &#13;&#10;');
                }

            },
            error: function(response){
                if(response.status === 422){
                    $.each(response.responseJSON.errors, function(key, value){
                        var name = $("#form-prueba-proveedor-sms [name='"+key+"']");
                        if(key.indexOf(".") != -1){
                            var arr = key.split(".");
                            name = $("#form-prueba-proveedor-sms [name='"+arr[0]+"[]']:eq("+arr[1]+")");
                        }
                        name.parent().append('<span class="error_c" style="color: red;">'+value[0]+'</span>');
                        name.focus();
                    });
                    toastr.info('Hubo un problema al validar tus datos');
                    hora = f.getHours() +':'+ f.getMinutes() +':'+ f.getSeconds();
                    $('#modal-prueba-proveedorSms #consoleLog').append(dia +' '+hora+' No se ha podido enviar la información  &#13;&#10;');
                    $('#modal-prueba-proveedorSms #consoleLog').append(dia +' '+hora+' Test finalizado &#13;&#10;');
                }else{
                    if(response.responseJSON.message){
                        toastr.error(response.responseJSON.message);
                    }else{
                        toastr.error('Se ha presentado un error al enviar los datos.');
                    }                    
                }
            },
            complete:function(){
                $('#mostrar_loading').removeClass('loader'); 
            }
        });
    });
    
    /*------------- Cuentas de Correo -------------*/

    /** Evento button que muestra la modal para crear un nuevo registro */
    $('#btn-crear-cuenta-correo').on('click', function(){

        $("#form-cuenta-correo")[0].reset();
        $('#modal-cuentaCorreo .modal-title').text("Registrar cuenta de correo");
        $("#form-cuenta-correo #accion").val('registrar');
        $('#modal-cuentaCorreo').modal('show');
    });
    
    /**
     * Evento button que muestra el modal para editar
     */
    $('#collapseCuentasCorreo table').on('click', '.btn-editar-cuenta-correo', function(){

        $("#form-cuenta-correo")[0].reset();
        $('#modal-cuentaCorreo .modal-title').text("Editar cuenta de correo");
        $("#form-cuenta-correo #accion").val('actualizar');
        $('#modal-cuentaCorreo').modal('show');
        
        $('#form-cuenta-correo .error_c').remove();
        $('#mostrar_loading').addClass('loader');

        var id = $(this).data('id');

        $.ajax({
            type: 'GET',
            url: 'huesped/cuenta-correo/'+id,
            success: function (response) {           
                editarCuentaCorreo(response.cuentaCorreo);
            },
            error: function(response) { 
                 toastr.error(response.message);
            },
            complete:function(){
                $('#mostrar_loading').removeClass('loader'); 
            }
        })
    });

    /** Llenado de datos proveedor automaticamente */
    $( "#form-cuenta-correo #proveedor" ).change(function() {
        let opcionProveedor = $(this).val();
        let cuentaCorreo = $('#form-cuenta-correo');
        let accion = $("#form-cuenta-correo #accion").val();

        if(accion == 'registrar'){
            switch (opcionProveedor) {
                case 'gmail':
                    cuentaCorreo.find('input#servidor_saliente_direccion').val('smtp.gmail.com');
                    cuentaCorreo.find('select#servidor_saliente_tipo').val(1);
                    cuentaCorreo.find('input#servidor_saliente_usar_auth').prop('checked', true);
                    cuentaCorreo.find('input#servidor_saliente_usar_start_ttls').prop('checked',true);
                    cuentaCorreo.find('input#servidor_saliente_usar_ssl').prop('checked', false);
                    cuentaCorreo.find('input#servidor_saliente_puerto').val(587);
                    cuentaCorreo.find('input#servidor_entrante_direccion').val('imap.gmail.com');
                    cuentaCorreo.find('select#servidor_entrante_tipo').val(3);
                    cuentaCorreo.find('input#servidor_entrante_usar_auth').prop('checked', true);
                    cuentaCorreo.find('input#servidor_entrante_usar_start_ttls').prop('checked', true);
                    cuentaCorreo.find('input#servidor_entrante_usar_ssl').prop('checked', false);
                    cuentaCorreo.find('input#servidor_entrante_puerto').val(993);
                    break;
    
                case 'microsoft':
                    cuentaCorreo.find('input#servidor_saliente_direccion').val('smtp.office365.com');
                    cuentaCorreo.find('select#servidor_saliente_tipo').val(1);
                    cuentaCorreo.find('input#servidor_saliente_usar_auth').prop('checked', true);
                    cuentaCorreo.find('input#servidor_saliente_usar_start_ttls').prop('checked', true);
                    cuentaCorreo.find('input#servidor_saliente_usar_ssl').prop('checked', false);
                    cuentaCorreo.find('input#servidor_saliente_puerto').val(587);
                    cuentaCorreo.find('input#servidor_entrante_direccion').val('imap.office365.com');
                    cuentaCorreo.find('select#servidor_entrante_tipo').val(3);
                    cuentaCorreo.find('input#servidor_entrante_usar_auth').prop('checked', false);
                    cuentaCorreo.find('input#servidor_entrante_usar_start_ttls').prop('checked', false);
                    cuentaCorreo.find('input#servidor_entrante_usar_ssl').prop('checked', true);
                    cuentaCorreo.find('input#servidor_entrante_puerto').val(993);
                    break;
                
                case 'otros':
                    cuentaCorreo.find('input#servidor_saliente_direccion').val('');
                    cuentaCorreo.find('select#servidor_saliente_tipo').val('');
                    cuentaCorreo.find('input#servidor_saliente_usar_auth').prop('checked', false);
                    cuentaCorreo.find('input#servidor_saliente_usar_start_ttls').prop('checked', false);
                    cuentaCorreo.find('input#servidor_saliente_usar_ssl').prop('checked', false);
                    cuentaCorreo.find('input#servidor_saliente_puerto').val('');
                    cuentaCorreo.find('input#servidor_entrante_direccion').val('');
                    cuentaCorreo.find('select#servidor_entrante_tipo').val('');
                    cuentaCorreo.find('input#servidor_entrante_usar_auth').prop('checked', false);
                    cuentaCorreo.find('input#servidor_entrante_usar_start_ttls').prop('checked', false);
                    cuentaCorreo.find('input#servidor_entrante_usar_ssl').prop('checked', false);
                    cuentaCorreo.find('input#servidor_entrante_puerto').val('');
                    break;
            
                default:
                    break;
            }
        }
    });

    /** Si el campo usuario esta vacio automaticamente se agrega el correo electronico */
    $("#form-cuenta-correo #direccion_correo_electronico").focusout(function(){
        let correo = $('#form-cuenta-correo #direccion_correo_electronico');
        let usuario = $('#form-cuenta-correo #usuario');
        
        if(usuario.val().length == 0){
            usuario.val(correo.val());
        }
    });

    /** Guarda el registro del formulario store y update */
    $('#modal-cuentaCorreo #btn-guardar-cuenta-correo').on('click', function(e){
        e.preventDefault();
        $('#mostrar_loading').addClass('loader');
        $('#form-cuenta-correo .error_c').remove();
        var data = new FormData($('#form-cuenta-correo')[0]);
        var token = $("input[name=_token]").val();
        var id = $('input#id').val();
        var accion = $('#form-cuenta-correo #accion').val();

        switch (accion) {
            case 'registrar':
                $.ajax({
                    url: 'huesped/crear-cuenta-correo/'+id,
                    headers: {'X-CSRF-TOKEN':token},
                    type: 'POST',
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data: data,
                    success: function(response){
                        toastr.success(response.message);
                        $("#form-cuenta-correo")[0].reset();
                        getCuentasCorreo(id);
                        $('#modal-cuentaCorreo').modal('hide');
                    },
                    error: function(response){
                        if(response.status === 422){
                            $.each(response.responseJSON.errors, function(key, value){
                                var name = $("#form-cuenta-correo [name='"+key+"']");
                                if(key.indexOf(".") != -1){
                                    var arr = key.split(".");
                                    name = $("#form-cuenta-correo [name='"+arr[0]+"[]']:eq("+arr[1]+")");
                                }
                                name.parent().append('<span class="error_c" style="color: red;">'+value[0]+'</span>');
                                name.focus();
                            });
                            toastr.info('Hubo un problema al validar tus datos');
                        }else{
                            if(response.responseJSON.message){
                                toastr.error(response.responseJSON.message);
                            }else{
                                toastr.error('Se ha presentado un error al guardar los datos.');
                            }                    
                        }
                    },
                    complete:function(){
                        $('#mostrar_loading').removeClass('loader'); 
                    }
                });
                break;
        
            case 'actualizar':
                data.append('_method', 'PUT');
                var id_registro = $('#form-cuenta-correo #cc_id').val();
                $.ajax({
                    url: 'huesped/actualizar-cuenta-correo/'+id_registro,
                    headers: {'X-CSRF-TOKEN':token},
                    type: 'POST',
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data: data,
                    success: function(response){
                        toastr.success(response.message);
                        getCuentasCorreo(response.cuentaCorreo.id_huesped);
                        $('#modal-cuentaCorreo').modal('hide');
                    },
                    error: function(response){
                        if(response.status === 422){
                            $.each(response.responseJSON.errors, function(key, value){
                                var name = $("#form-cuenta-correo [name='"+key+"']");
                                if(key.indexOf(".") != -1){
                                    var arr = key.split(".");
                                    name = $("#form-cuenta-correo [name='"+arr[0]+"[]']:eq("+arr[1]+")");
                                }
                                name.parent().append('<span class="error_c" style="color: red;">'+value[0]+'</span>');
                                name.focus();
                            });
                            toastr.info('Hubo un problema al validar tus datos');
                        }else{
                            if(response.responseJSON.message){
                                toastr.error(response.responseJSON.message);
                            }else{
                                toastr.error('Se ha presentado un error al guardar los datos.');
                            }                    
                        }
                    },
                    complete:function(){
                        $('#mostrar_loading').removeClass('loader'); 
                    }
                });
                break;
            default:
                break;
        }
    });

    /** Elimina el registro seleccionado */
    $('#collapseCuentasCorreo table').on('click', '.btn-eliminar-cuenta-correo', function(){
        var id = $(this).data('id');
        var id_huesped = $('input#id').val();
        var token = $("input[name=_token]").val();

        Swal.fire({
            title: 'Deseas eliminar esta cuenta de correo?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminar!'
        }).then((result) => {
            if (result.value) {
                $('#mostrar_loading').addClass('loader');
                $.ajax({
                    headers: {'X-CSRF-TOKEN':token},
                    type: 'DELETE',
                    url: 'huesped/eliminar-cuenta-correo/'+id,
                    data: {
                        "id": id
                    },
                    success: function (response) {           
                        getCuentasCorreo(id_huesped);
                        Swal.fire(
                            'Eliminado!',
                            'Su registro ha sido eliminado.',
                            'success'
                        )
                    },
                    error: function(response) { 
                         toastr.error(response.responseJSON.message);
                    },
                    complete:function(){
                        $('#mostrar_loading').removeClass('loader'); 
                    }
                })
              
            }
        })
    });
   
    /**Busca todos las cuentas de correo perteneciente a un huesped */
    function getCuentasCorreo(id){

        $.ajax({
            type: 'GET',
            url: 'huesped/listar-cuentas-correo/'+id,
            success: function (response) {
                let html11 = '';
                if(response.cuentasCorreo.length > 0){    
                    $.each(response.cuentasCorreo, function(key, value){
                        html11 += agregarCuentaCorreo(value);
                    });
                }else{
                    html11 = `
                    <tr>
                        <td colspan="5"><h4>El huésped aún no tiene cuentas de correo</h4></td>
                    </tr>
                    `;
                }
                
                 $('#collapseCuentasCorreo table tbody').html(html11);      
            },
            error: function(response) { 
                 toastr.error("No se pudo obtener las cuentas de correo del huésped");
            }
        })
    };

    /** Agrega al formulario la data de la cuenta de correo seleccionada */
    function editarCuentaCorreo(data){

        let cuentaCorreo = $('#form-cuenta-correo');

        cuentaCorreo.find('input#cc_id').val(data.id);
        cuentaCorreo.find('input#nombre').val(data.nombre);
        cuentaCorreo.find('input#direccion_correo_electronico').val(data.direccion_correo_electronico);
        cuentaCorreo.find('input#servidor_saliente_direccion').val(data.servidor_saliente_direccion);
        cuentaCorreo.find('select#servidor_saliente_tipo').val(data.servidor_saliente_tipo);
        cuentaCorreo.find('input#servidor_saliente_usar_auth').prop('checked', (data.servidor_saliente_usar_auth == 1) ? true : false);
        cuentaCorreo.find('input#servidor_saliente_usar_start_ttls').prop('checked', (data.servidor_saliente_usar_start_ttls == 1) ? true : false);
        cuentaCorreo.find('input#servidor_saliente_usar_ssl').prop('checked', (data.servidor_saliente_usar_ssl == 1) ? true : false);
        cuentaCorreo.find('input#servidor_saliente_puerto').val(data.servidor_saliente_puerto);
        cuentaCorreo.find('input#saliente_responder_a').val(data.saliente_responder_a);
        cuentaCorreo.find('input#saliente_nombre_remitente').val(data.saliente_nombre_remitente);
        cuentaCorreo.find('input#servidor_entrante_direccion').val(data.servidor_entrante_direccion);
        cuentaCorreo.find('select#servidor_entrante_tipo').val(data.servidor_entrante_tipo);
        cuentaCorreo.find('input#servidor_entrante_usar_auth').prop('checked', (data.servidor_entrante_usar_auth == 1) ? true : false);
        cuentaCorreo.find('input#servidor_entrante_usar_start_ttls').prop('checked', (data.servidor_entrante_usar_start_ttls == 1) ? true : false);
        cuentaCorreo.find('input#servidor_entrante_usar_ssl').prop('checked', (data.servidor_entrante_usar_ssl == 1) ? true : false);
        cuentaCorreo.find('input#servidor_entrante_puerto').val(data.servidor_entrante_puerto);
        cuentaCorreo.find('input#usuario').val(data.usuario);
        cuentaCorreo.find('input#borrar_correos_procesados').prop('checked', (data.borrar_correos_procesados == 1) ? true : false);
        cuentaCorreo.find('select#estado').val(data.estado);
        cuentaCorreo.find('input#mensajes_estado').val(data.mensajes_estado);
        cuentaCorreo.find('input#intervalo_refresque').val(data.intervalo_refresque);
        cuentaCorreo.find('input#buzon').val(data.buzon);
        cuentaCorreo.find('select#proveedor').val(data.proveedor);
    }

    /** Actualiza la tabla de las cuentas de correo del huesped */
    function agregarCuentaCorreo(data){
        let html = `
        <tr>
            <td>${data.nombre}</td>
            <td>${data.proveedor}</td>
            <td>${data.direccion_correo_electronico}</td>
            <td>${data.estado == 1 ? 'activo':'detenido'}</td>
            <td>
                <button data-id="${data.id}" class="btn btn-primary btn-sm btn-editar-cuenta-correo" title="Editar">
                    <i class="fa fa-edit"></i>
                </button>
                <button data-id="${data.id}" class="btn btn-danger btn-sm btn-eliminar-cuenta-correo" title="Eliminar">
                    <i class="fa fa-trash"></i>
                </button>
                <button data-id="${data.id}" data-huesped="${data.id_huesped}" data-usuario="${data.usuario}" data-nombre="${data.direccion_correo_electronico}" class="btn btn-info btn-sm btn-test-cuenta">Prueba</button>
            </td>
        </tr>
        `;
        
        return html;
    }

    /** Muestra la modal para hacer la prueba de cuentas de correo */
    $('#collapseCuentasCorreo table').on('click', '.btn-test-cuenta', function(){
        
        $("#form-test-cuenta-correo")[0].reset();
        $('#modal-test-cuentaCorreo').modal('show');

        $('#modal-test-cuentaCorreo .estado-test-mail-entrada').hide();
        $('#modal-test-cuentaCorreo .estado-test-mail-salida').hide();
        
        $('#modal-test-cuentaCorreo #consoleLogCorreo').hide();

        $('#form-test-cuenta-correo .error_c').remove();

        var id = $(this).data('id');
        var id_huesped = $(this).data('huesped');
        var usuario = $(this).data('usuario');
        var nombre = $(this).data('nombre');

        $('#form-test-cuenta-correo #id_cuentaCorreo').val(id);
        $('#form-test-cuenta-correo #cuentaCorreoUsuario').val(usuario);
        $('#form-test-cuenta-correo #id_huesped').val(id_huesped);
        $('#form-test-cuenta-correo .cuenta-correo-nombre').text(nombre);
    });

    /** Ejectuta la prueba para la cuentas de correo */
    $('#modal-test-cuentaCorreo #btn-test-cuenta').on('click', function(){
        $('#mostrar_loading').addClass('loader');
        $('#form-test-cuenta-correo .error_c').remove();
        var data = new FormData($('#form-test-cuenta-correo')[0]);
        
        $('#modal-test-cuentaCorreo #consoleLogCorreo').html('');
        $('#modal-test-cuentaCorreo #consoleLogCorreo').show();

        testSendMailService(data);
    });

    function testSendMailService(data1){
        var data = data1;
        var token = $("input[name=_token]").val();

        var f = new Date();

        var dia = f.getDate() +'-'+ (f.getMonth() + 1) +'-'+ f.getFullYear();
        var hora = f.getHours() +':'+ f.getMinutes() +':'+ f.getSeconds();

        $('#modal-test-cuentaCorreo #consoleLogCorreo').append('Prueba cuenta de correo &#13;&#10;');
        $('#modal-test-cuentaCorreo #consoleLogCorreo').append(dia +' '+hora+' Prueba de correo de salida iniciado  &#13;&#10;'); 

        $.ajax({
            url: 'huesped/test-correo-send-mail-service',
            headers: {'X-CSRF-TOKEN':token},
            type: 'POST',
            dataType: 'json',
            processData: false,
            contentType: false,
            data: data,
            success: function(response){
                
                $('#modal-test-cuentaCorreo .estado-test-mail-salida').text('Prueba de salida fallida').removeClass('text-success').addClass('text-danger').show();
                if(response.json){

                    if(response.json.strEstado_t == 'ok'){
                        $('#modal-test-cuentaCorreo #consoleLogCorreo').append(dia +' '+hora+' Estado: '+response.json.strEstado_t+' &#13;&#10;');
                        $('#modal-test-cuentaCorreo #consoleLogCorreo').append(dia +' '+hora+' Se ha ejecutado la prueba de correo de salida exitosamente &#13;&#10;');
                        $('#modal-test-cuentaCorreo .estado-test-mail-salida').text('Prueba de salida exitosa').removeClass('text-danger').addClass('text-success');
                    }else{
                        hora = f.getHours() +':'+ f.getMinutes() +':'+ f.getSeconds();
                        $('#modal-test-cuentaCorreo #consoleLogCorreo').append(dia +' '+hora+' Estado: '+response.json.strEstado_t+' &#13;&#10;');
                        $('#modal-test-cuentaCorreo #consoleLogCorreo').append(dia +' '+hora+' Hubo un error al ejecutar la prueba de salida de correo &#13;&#10;');
                    }

                    
                    //toastr.success(response.message + response.json.strStatus_t);

                }else{
                    $('#modal-test-cuentaCorreo #consoleLogCorreo').append(dia +' '+hora+' Ha habido un problema al comunicarse con la api send mail &#13;&#10;');
                }

                testEntrada(data);
            },
            error: function(response){
                if(response.status === 422){
                    $.each(response.responseJSON.errors, function(key, value){
                        var name = $("#form-test-cuenta-correo [name='"+key+"']");
                        if(key.indexOf(".") != -1){
                            var arr = key.split(".");
                            name = $("#form-test-cuenta-correo [name='"+arr[0]+"[]']:eq("+arr[1]+")");
                        }
                        name.parent().append('<span class="error_c" style="color: red;">'+value[0]+'</span>');
                        name.focus();
                    });
                    toastr.info('Hubo un problema al validar tus datos');
                    hora = f.getHours() +':'+ f.getMinutes() +':'+ f.getSeconds();
                    $('#modal-test-cuentaCorreo #consoleLogCorreo').append(dia +' '+hora+' No se ha podido enviar la información  &#13;&#10;');
                    $('#modal-test-cuentaCorreo #consoleLogCorreo').append(dia +' '+hora+' Prueba finalizada &#13;&#10;');
                }else if(response.status === 502){
                    toastr.error('Se ha presentado un error en el envio de email.');
                    $('#mostrar_loading').removeClass('loader');
                }
                else if(response.status === 504){
                    toastr.error('Se ha presentado un error en el envio de email.');
                    $('#mostrar_loading').removeClass('loader');
                }
                else{
                    if(response.responseJSON.message){
                        toastr.error(response.responseJSON.message);
                    }else{
                        toastr.error('Se ha presentado un error al enviar los datos.');
                    }                    
                }

                $('#mostrar_loading').removeClass('loader'); 
            },
            complete:function(){
                //$('#mostrar_loading').removeClass('loader'); 
            }
        });
    }

    function testEntrada(data1){

        var data = data1;
        var token = $("input[name=_token]").val();

        var f = new Date();

        var dia = f.getDate() +'-'+ (f.getMonth() + 1) +'-'+ f.getFullYear();
        var hora = f.getHours() +':'+ f.getMinutes() +':'+ f.getSeconds();

        $('#modal-test-cuentaCorreo #consoleLogCorreo').append(dia +' '+hora+' Prueba de correo de entrada iniciado &#13;&#10;');
        
        $.ajax({
            url: 'huesped/test-correo-in',
            headers: {'X-CSRF-TOKEN':token},
            type: 'POST',
            dataType: 'json',
            processData: false,
            contentType: false,
            data: data,
            success: function(response){
                
                $('#modal-test-cuentaCorreo .estado-test-mail-entrada').text('Prueba de entrada fallida').removeClass('text-success').addClass('text-danger').show();
                if(response.json){

                    if(response.json.strEstado_t == 'ok'){
                        $('#modal-test-cuentaCorreo #consoleLogCorreo').append(dia +' '+hora+' Se ha ejecutado la prueba de entrada de correo exitosamente &#13;&#10;');
                        $('#modal-test-cuentaCorreo #consoleLogCorreo').append(dia +' '+hora+' Prueba de correo de entrada finalizada &#13;&#10;');
                        $('#modal-test-cuentaCorreo #consoleLogCorreo').append(dia +' '+hora+' Prueba finalizada &#13;&#10;');
                        $('#modal-test-cuentaCorreo .estado-test-mail-entrada').text('Prueba de entrada exitosa').removeClass('text-danger').addClass('text-success');
                    }else{
                        hora = f.getHours() +':'+ f.getMinutes() +':'+ f.getSeconds();
                        $('#modal-test-cuentaCorreo #consoleLogCorreo').append(dia +' '+hora+' Estado: '+response.json.strEstado_t+' &#13;&#10;');
                        $('#modal-test-cuentaCorreo #consoleLogCorreo').append(dia +' '+hora+' Hubo un error al ejecutar la prueba de entrada de correo &#13;&#10;');
                    }
                    //toastr.success(response.message + response.json.strStatus_t);

                }else{
                    $('#modal-test-cuentaCorreo #consoleLogCorreo').append(dia +' '+hora+' Ha habido un problema al comunicarse con la api de mail entrada &#13;&#10;');
                }

            },
            error: function(response){
                if(response.status === 422){
                    $.each(response.responseJSON.errors, function(key, value){
                        var name = $("#form-test-cuenta-correo [name='"+key+"']");
                        if(key.indexOf(".") != -1){
                            var arr = key.split(".");
                            name = $("#form-test-cuenta-correo [name='"+arr[0]+"[]']:eq("+arr[1]+")");
                        }
                        name.parent().append('<span class="error_c" style="color: red;">'+value[0]+'</span>');
                        name.focus();
                    });
                    toastr.info('Hubo un problema al validar tus datos');
                    hora = f.getHours() +':'+ f.getMinutes() +':'+ f.getSeconds();
                    $('#modal-test-cuentaCorreo #consoleLogCorreo').append(dia +' '+hora+' No se ha podido enviar la información  &#13;&#10;');
                    $('#modal-test-cuentaCorreo #consoleLogCorreo').append(dia +' '+hora+' Prueba finalizada &#13;&#10;');
                }else if(response.status === 502){
                    toastr.error('Se ha presentado un error en el envio de email.');
                    $('#mostrar_loading').removeClass('loader');
                    $('#modal-test-cuentaCorreo .estado-test-mail-entrada').text('Prueba de entrada fallida').removeClass('text-success').addClass('text-danger').show();
                    $('#modal-test-cuentaCorreo #consoleLogCorreo').append(dia +' '+hora+' No se pudo recibir respuesta del servidor  &#13;&#10;');
                }
                else if(response.status === 504){
                    toastr.error('Se ha presentado un error en el envio de email.');
                    $('#mostrar_loading').removeClass('loader');
                    $('#modal-test-cuentaCorreo .estado-test-mail-entrada').text('Prueba de entrada fallida').removeClass('text-success').addClass('text-danger').show();
                    $('#modal-test-cuentaCorreo #consoleLogCorreo').append(dia +' '+hora+' No se pudo recibir respuesta del servidor  &#13;&#10;');
                }else{
                    if(response.responseJSON.message){
                        toastr.error(response.responseJSON.message);
                    }else{
                        toastr.error('Se ha presentado un error al enviar los datos.');
                    }                    
                }
            },
            complete:function(){
                $('#mostrar_loading').removeClass('loader'); 
            }
        });
    }

    $('#form-prueba-proveedor-sms').submit(function(e){
        e.preventDefault();
    });

    $('#form-test-cuenta-correo').submit(function(e){
        e.preventDefault();
    });

    /*--------------------------------- Troncales  ---------------------------------*/
    
    /** Retorna una fila de la troncal */
    function getHtmlTroncal(data){
        let html = `
        <tr>
            <td>${data.nombre_usuario}</td>
            <td>${data.codigo_cuenta}</td>
            <td id="estadoTroncal${data.id}"><spam class="label label-info">Cargando...</spam></td>
            <td>
                <button data-id="${data.id}" type="button" class="btn btn-primary btn-sm btn-editar-troncal" title="Editar"><i class="fa fa-edit"></i></button>
                <button data-id="${data.id}" type="button" class="btn btn-danger btn-sm btn-eliminar-troncal" title="Eliminar"><i class="fa fa-trash"></i></button>
             </td>
        </tr>
        `;
        
        return html;
    }
    
    /** Esta funcion retorna la tabla con todas las troncales del huesped */
    function getTroncales(id){
    
        $.ajax({
            type: 'GET',
            url: 'huesped/listar-troncales/'+id,
            success: function (response) {
                let html11 = '';
                if(response.troncales.length > 0){    
                    $.each(response.troncales, function(key, value){
                        html11 += getHtmlTroncal(value);
                    });
                }else{
                    html11 = `
                    <tr>
                        <td colspan="5"><h4>El huésped aún no tiene troncales</h4></td>
                    </tr>
                    `;
                }
                
                $('#collapseTroncales table tbody').html(html11);      
                getEstadosTroncales(id);
            },
            error: function(response) { 
                 toastr.error("No se pudo obtener las cuentas de correo del huésped");
            }
        })
    };
    
    function getEstadosTroncales(id_huesped){
        $.ajax({
            type: 'GET',
            url: 'huesped/estado-troncal/'+id_huesped,
            success: function (response) {           
                
                for (const [key, value] of Object.entries(response.troncalEstado)) {
                    
                    let my_html = '';
                    switch (value) {
                        case 'ok':
                            my_html = '<spam class="label label-success">'+value+'</spam>';
                            break;
                        case 'no_configurada':
                                my_html = '<spam class="label label-primary">'+value+'</spam>';
                            break;
                        case 'error':
                                my_html = '<spam class="label label-danger">'+value+'</spam>';
                            break;
                        case 'no_existe':
                            my_html = '<spam class="label label-danger">'+value+'</spam>';
                            break;
                        case 'no_autorizado':
                            my_html = '<spam class="label label-default">'+value+'</spam>';
                            break;
                        case 'error_api':
                                my_html = '<spam class="label label-danger">'+value+'</spam>';
                            break;
                        default:
                                my_html = '<spam class="label label-default">'+value+'</spam>';
                            break;
                    }
                    $('#estadoTroncal'+key).html(my_html);
                }
            },
            error: function(response) { 
                if(response.responseJSON.message){
                    toastr.error(response.responseJSON.message);
                }else{
                    toastr.error('Se ha presentado un error al consultar los estados de las troncales.');
                } 
            }
        })
    }
    
    /** Llena el formulario de troncal con los datoas recibidos para la edicion */
    function editarTroncal(data){
        let troncal = $('#form-troncal');
    
        troncal.find('input#id_troncal').val(data.id);
        troncal.find('input#troncal_nombre_usuario').val(data.nombre_usuario);
        troncal.find('input#troncal_codigo_cuenta').val(data.codigo_cuenta);
        troncal.find('input#troncal_usar_codigo_antepuesto').prop('checked', (data.usar_codigo_antepuesto == 1) ? true : false);
    
        // En esta seccion inserta las propiedades de la troncal
    
        data.configuraciones.forEach( item => {
            switch (item.id_propiedad) {
                case 51:
                    troncal.find('select#troncal_tipo').val(item.valor);
                    break;
                case 43:
                    troncal.find('input#troncal_direccion_servidor').val(item.valor);
                    break;
                case 39:
                    troncal.find('input#troncal_usuario_defecto').val(item.valor);
                    break;
                case 42:
                    troncal.find('input#troncal_fuente').val(item.valor);
                    break;
                case 49:
                    troncal.find('input#troncal_contrasena').val(item.valor);
                    break;
                case 48:
                    troncal.find('select#troncal_compensar_rfc2833').val(item.valor);
                    break;
                case 35:
                    troncal.find('input#troncal_limite_llamadas').val(item.valor);
                    break;
                case 38:
                    troncal.find('input#troncal_contexto').val(item.valor);
                    break;
                case 37:
                    troncal.find('select#troncal_habilitar_puente_rtp').val(item.valor);
                    break;
                case 52:
                    troncal.find('input#troncal_autenticacion').val(item.valor);
                    break;
                case 44:
                    troncal.find('select#troncal_nat').val(item.valor);
                    break;
                case 47:
                    troncal.find('select#troncal_permitir_verificacion').val(item.valor);
                    break;
                case 68:
                    troncal.find('input#troncal_codec_u').prop('checked', (item.valor == 1) ? true : false);
                    break;
                case 69:
                    troncal.find('input#troncal_codec_a').prop('checked', (item.valor == 1) ? true : false);
                    break;
                case 70:
                    troncal.find('input#troncal_g729').prop('checked', (item.valor == 1) ? true : false);
                    break;
                default:
                    break;
            }
        });
    }
    
    /** Este boton despliega una modal para registrar una troncal */
    $('#btn-agregar-troncal').on('click', function(){
        $("#form-troncal")[0].reset();
        $('#modal-troncal .modal-title').text("Registrar troncal");
        $("#form-troncal #accion").val('registrar');
        $('#form-troncal .error_c').remove();
        $('#modal-troncal').modal('show');
    });
    
    /** Guardar el registro de l formulario de troncal strore y update */
    $('#modal-troncal #btn-guardar-troncal').on('click', function(e){
        e.preventDefault();
    
        $('#mostrar_loading').addClass('loader');
        $('#form-troncal .error_c').remove();
    
        var data = new FormData($('#form-troncal')[0]);
        var token = $("input[name=_token]").val();
        var id_huesped = $('input#id').val();
        var accion = $('#form-troncal #accion').val();
        
        switch (accion) {
            case 'registrar':
                $.ajax({
                    url: 'huesped/crear-troncal/'+id_huesped,
                    headers: {'X-CSRF-TOKEN':token},
                    type: 'POST',
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data: data,
                    success: function(response){
                        toastr.success(response.message);
                        $("#form-troncal")[0].reset();
                        getTroncales(id_huesped);
                        $('#modal-troncal').modal('hide');
                    },
                    error: function(response){
                        if(response.status === 422){
                            $.each(response.responseJSON.errors, function(key, value){
                                var name = $("#form-troncal [name='"+key+"']");
                                if(key.indexOf(".") != -1){
                                    var arr = key.split(".");
                                    name = $("#form-troncal [name='"+arr[0]+"[]']:eq("+arr[1]+")");
                                }
                                name.parent().append('<span class="error_c" style="color: red;">'+value[0]+'</span>');
                                name.focus();
                            });
                            toastr.info('Hubo un problema al validar tus datos');
                        }else{
                            if(response.responseJSON.message){
                                toastr.error(response.responseJSON.message);
                            }else{
                                toastr.error('Se ha presentado un error al guardar los datos.');
                            }  
                        }
                    },
                    complete: function(){
                        $('#mostrar_loading').removeClass('loader');
                    }
                });
                break;
    
            case 'actualizar':
                data.append('_method', 'PUT');
                var id_troncal = $('#form-troncal #id_troncal').val();
    
                $.ajax({
                    url: 'huesped/actualizar-troncal/'+id_troncal,
                    headers: {'X-CSRF-TOKEN':token},
                    type: 'POST',
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data: data,
                    success: function(response){
                        toastr.success(response.message);
                        getTroncales(id_huesped);
                        $('#modal-troncal').modal('hide');
                    },
                    error: function(response){
                        if(response.status === 422){
                            $.each(response.responseJSON.errors, function(key, value){
                                var name = $("#form-troncal [name='"+key+"']");
                                if(key.indexOf(".") != -1){
                                    var arr = key.split(".");
                                    name = $("#form-troncal [name='"+arr[0]+"[]']:eq("+arr[1]+")");
                                }
                                name.parent().append('<span class="error_c" style="color: red;">'+value[0]+'</span>');
                                name.focus();
                            });
                            toastr.info('Hubo un problema al validar tus datos');
                        }else{
                            if(response.responseJSON.message){
                               toastr.error(response.responseJSON.message);
                            }else{
                                toastr.error('Se ha presentado un error al guardar los datos.');
                            }
                         }
                    },
                    complete:function(){
                        $('#mostrar_loading').removeClass('loader'); 
                    }
                });
                break;
            default:
                break;
        }
    });
    
    /** Este evento abre la modal y trae el registro de la troncal para poderla editar */
    $('#collapseTroncales table').on('click', '.btn-editar-troncal', function(){
    
        $("#form-troncal")[0].reset();
        $('#modal-troncal .modal-title').text("Editar troncal");
        $("#form-troncal #accion").val('actualizar');
        $('#modal-troncal').modal('show');
        
        $('#form-troncal .error_c').remove();
        $('#mostrar_loading').addClass('loader');
    
        var id = $(this).data('id');
    
        $.ajax({
            type: 'GET',
            url: 'huesped/troncal/'+id,
            success: function (response) {           
                editarTroncal(response.troncal);
                
            },
            error: function(response) { 
                 toastr.error(response.message);
            },
            complete:function(){
                $('#mostrar_loading').removeClass('loader'); 
            }
        })
    });
    
    /** Elimina el registro de la troncal */
    $('#collapseTroncales table').on('click', '.btn-eliminar-troncal', function(){
        var id_troncal = $(this).data('id');
        var id_huesped = $('input#id').val();
        var token = $("input[name=_token]").val();
    
        Swal.fire({
            title: 'Deseas eliminar esta troncal?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminar!'
        }).then((result) => {
            if (result.value) {
                $('#mostrar_loading').addClass('loader');
                $.ajax({
                    headers: {'X-CSRF-TOKEN':token},
                    type: 'DELETE',
                    url: 'huesped/eliminar-troncal/'+id_troncal,
                    data: {
                        "id_troncal": id_troncal
                    },
                    success: function (response) {           
                        getTroncales(id_huesped);
                        Swal.fire(
                            'Eliminado!',
                            'Su registro ha sido eliminado.',
                            'success'
                        )
                    },
                    error: function(response) { 
                         toastr.error(response.responseJSON.message);
                    },
                    complete:function(){
                        $('#mostrar_loading').removeClass('loader'); 
                    }
                })
              
            }
        })
    });

    /** ----------------------- Usuarios ---------------------------  */
    /** Retorna una fila de usuario */
    function getHtmlUsuario(data){
        let html = `
        <tr>
            <td>${data.USUARI_Nombre____b}</td>
            <td>${data.USUARI_Correo___b}</td>
            <td>
                <button data-id_huesped="${data.id_huesped}" data-id_usuario="${data.id_usuario}" type="button" class="btn btn-danger btn-sm btn-eliminar-usuario" title="Desvincular"><i class="fa fa-chain-broken"></i></button>
             </td>
        </tr>
        `;
        
        return html;
    }

    /** Esta funcion retorna la tabla con todos los usuarios asignados al huesped */
    function getUsuarios(id){
    
        $.ajax({
            type: 'GET',
            url: 'huesped/listar-usuarios/'+id,
            success: function (response) {
                let html11 = '';
                if(response.usuarios.length > 0){    
                    $.each(response.usuarios, function(key, value){
                        html11 += getHtmlUsuario(value);
                    });
                }else{
                    html11 = `
                    <tr>
                        <td colspan="5"><h4>El huésped aún no tiene usuarios asignados</h4></td>
                    </tr>
                    `;
                }
                
                 $('#collapseUsuarios table tbody').html(html11);      
            },
            error: function(response) { 
                 toastr.error("No se pudo obtener las cuentas de correo del huésped");
            }
        })
    };

    /** Abre la modal para hacer la asignacion de usuarios */
    $('#btn-asignar-usuario').on('click', function(){
        $("#form-usuario-asignar")[0].reset();
        $('#modal-usuario-asignar .modal-title').text("Asignar un usuario al huesped");
        $('#modal-usuario-asignar').modal('show');

        $.ajax({
            type: 'GET',
            url: 'listar-usuarios-admin/',
            success: function (response) {
                let html11 = '<option value="">Seleccionar</option>';
                if(response.usuarios.length > 0){    
                    $.each(response.usuarios, function(key, value){
                        html11 += '<option value="'+value.USUARI_UsuaCBX___b+'">'+value.USUARI_Correo___b+'</option>';
                    });
                }                
                 $('#modal-usuario-asignar #usuarioAsignar').html(html11);      
            },
            error: function(response) { 
                 toastr.error("No se pudo obtener las cuentas de usuario");
            }
        })
    });
    
    /** Registra el usuario seleccionado al huesped */
    $('#modal-usuario-asignar #btn-guardar-usuario-asignacion').on('click', function(e){
        e.preventDefault();
    
        $('#mostrar_loading').addClass('loader');
        $('#form-usuario-asignar .error_c').remove();
    
        var data = new FormData($('#form-usuario-asignar')[0]);
        var token = $("input[name=_token]").val();
        var id_huesped = $('input#id').val();
        
        $.ajax({
            url: 'huesped/asignar-usuario/'+id_huesped,
            headers: {'X-CSRF-TOKEN':token},
            type: 'POST',
            dataType: 'json',
            processData: false,
            contentType: false,
            data: data,
            success: function(response){
                toastr.success(response.message);
                getUsuarios(id_huesped);
                $('#modal-usuario-asignar').modal('hide');
            },
            error: function(response){
                if(response.status === 422){
                    $.each(response.responseJSON.errors, function(key, value){
                        var name = $("#form-usuario-asignar [name='"+key+"']");
                        if(key.indexOf(".") != -1){
                            var arr = key.split(".");
                            name = $("#form-usuario-asignar [name='"+arr[0]+"[]']:eq("+arr[1]+")");
                        }
                        name.parent().append('<span class="error_c" style="color: red;">'+value[0]+'</span>');
                        name.focus();
                    });
                    toastr.info('Hubo un problema al validar tus datos');
                }else{
                    if(response.responseJSON.message){
                        toastr.error(response.responseJSON.message);
                    }else{
                        toastr.error('Se ha presentado un error al guardar los datos.');
                    }  
                }
            },
            complete: function(){
                $('#mostrar_loading').removeClass('loader');
            }
        });
    });

    /** Abre la modal para crear un usuario al huesped seleccionado */
    $('#btn-crear-usuario').on('click', function(){
        $("#form-usuario-crear")[0].reset();//**aqui quede */
        $('#modal-usuario-crear .modal-title').text("Crear usuario");
        $('#form-usuario-crear .error_c').remove();
        $('#modal-usuario-crear').modal('show');
    });

    /**Guarda el usuario */
    $('#btn-store-usuario').on('click', function(e){
        e.preventDefault();
    
        $('#mostrar_loading').addClass('loader');
        $('#form-usuario-crear .error_c').remove();
    
        var data = new FormData($('#form-usuario-crear')[0]);
        var token = $("input[name=_token]").val();
        var id_huesped = $('input#id').val();
        
        $.ajax({
            url: 'huesped/crear-usuario/'+id_huesped,
            headers: {'X-CSRF-TOKEN':token},
            type: 'POST',
            dataType: 'json',
            processData: false,
            contentType: false,
            data: data,
            success: function(response){
                $("#form-usuario-crear")[0].reset();
                toastr.success(response.message);
                getUsuarios(id_huesped);
                $('#modal-usuario-crear').modal('hide');
            },
            error: function(response){
                if(response.status === 422){
                    $.each(response.responseJSON.errors, function(key, value){
                        var name = $("#form-usuario-crear [name='"+key+"']");
                        if(key.indexOf(".") != -1){
                            var arr = key.split(".");
                            name = $("#form-usuario-crear [name='"+arr[0]+"[]']:eq("+arr[1]+")");
                        }
                        name.parent().append('<span class="error_c" style="color: red;">'+value[0]+'</span>');
                        name.focus();
                    });
                    toastr.info('Hubo un problema al validar tus datos');
                }else{
                    if(response.responseJSON.message){
                        toastr.error(response.responseJSON.message);
                    }else{
                        toastr.error('Se ha presentado un error al guardar los datos.');
                    }  
                }
            },
            complete: function(){
                $('#mostrar_loading').removeClass('loader');
            }
        });
    });

    /** Desvicula el usuario del huesped */
    $('#collapseUsuarios table').on('click', '.btn-eliminar-usuario', function(){
        var id_huesped = $(this).data('id_huesped');
        var id_usuario = $(this).data('id_usuario');
        var token = $("input[name=_token]").val();

        Swal.fire({
            title: 'Desea desvincular este usuario del huésped?',
            text: 'Al realizar esta operación solo se desvinculará el usuario del huésped, el registro del usuario no se eliminará.',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar', 
            confirmButtonText: 'Si, desvincular!'
        }).then((result) => {
            if (result.value) {
                $('#mostrar_loading').addClass('loader');
                $.ajax({
                    headers: {'X-CSRF-TOKEN':token},
                    type: 'DELETE',
                    url: 'huesped/eliminar-usuario/'+id_usuario,
                    data: {
                        "id_huesped": id_huesped, "id_usuario": id_usuario
                    },
                    success: function (response) {           
                        getUsuarios(id_huesped);

                        var titulo = 'Desvincular';
                        if(response.status == 'info'){
                            titulo = 'Denegado';
                        }
                        Swal.fire(
                            titulo,
                            response.message,
                            response.status
                        )
                    },
                    error: function(response) { 
                         toastr.error(response.responseJSON.message);
                    },
                    complete:function(){
                        $('#mostrar_loading').removeClass('loader'); 
                    }
                })
              
            }
        })        
    });

});

