<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/pais/ciudad/{id}', 'PaisCiudadController@getCiudad');
Route::get('/pais/ciudades/{pais}', 'PaisCiudadController@getCiudadPais');
Route::get('/pais', 'PaisCiudadController@getPais');

Route::get('/huesped/search', 'HuespedController@search')->name('huesped.search');
Route::get('/huesped/file/{id}/{tipo}', 'HuespedController@getFile');
Route::post('/huesped/add-new-contacto', 'HuespedController@addNewContacto');
Route::resource('/huesped', 'HuespedController');

// Pausas 
Route::get('/huesped/listar-pausas/{id_huesped}', 'PausaController@listarPausas');
Route::post('/huesped/guardar-pausas/{id_huesped}', 'PausaController@storePausa');

// Festivos y dias no festivos
Route::get('/huesped/listar-festivos/{id_huesped}', 'FestivosController@listarFestivos');
Route::post('/huesped/guardar-festivos/{id_huesped}', 'FestivosController@storeFestivos');

// Mail notificaciones internas
Route::get('/huesped/mail-notificaciones-internas/{id_huesped}', 'MailNotificacionesController@showMailNotificacion');
Route::post('/huesped/registrar-mail-notificaciones/{id_huesped}', 'MailNotificacionesController@insertarMailNotificacion');

// Notificaciones Internas
Route::post('/huesped/prueba-email-notificacion-smtp/{idEmail}', 'HuespedController@testCuentaNotificaciones');

// Proveedor Sms
Route::get('/huesped/proveedor-sms/{id}', 'ProveedorSmsController@ShowProveedorSms');
Route::get('/huesped/listar-proveedores-sms/{id_huesped}', 'ProveedorSmsController@showAllProveedorSms');
Route::post('/huesped/crear-proveedor-sms/{id_huesped}', 'ProveedorSmsController@storeProveedorSms');
Route::put('/huesped/actualizar-proveedor-sms/{id}', 'ProveedorSmsController@updateProveedorSms');
Route::delete('/huesped/eliminar-proveedor-sms/{id}', 'ProveedorSmsController@deleteProveedorSms');
Route::post('/huesped/prueba-proveedor-sms', 'ProveedorSmsController@pruebaEnviarSms');

// Seccion Cuentas de correo
Route::get('/huesped/cuenta-correo/{id}', 'CuentaCorreoController@showCuentaCorreo');
Route::get('/huesped/listar-cuentas-correo/{id}', 'CuentaCorreoController@showAllCuentaCorreo');
Route::post('/huesped/crear-cuenta-correo/{id}', 'CuentaCorreoController@storeCuentaCorreo')->name('canal.cuentaCorreo');
Route::put('/huesped/actualizar-cuenta-correo/{id}', 'CuentaCorreoController@updateCuentaCorreo');
Route::delete('/huesped/eliminar-cuenta-correo/{id}', 'CuentaCorreoController@deleteCuentaCorreo');
Route::post('/huesped/test-correo-send-mail-service', 'CuentaCorreoController@testSendMailService');
Route::post('/huesped/test-correo-in', 'CuentaCorreoController@testEntrada');

// Seccion troncales
Route::get('/huesped/estado-troncal/{id_huesped}', 'TroncalController@getEstadoTroncales');
Route::get('/huesped/listar-troncales/{id_huesped}', 'TroncalController@getTroncales');
Route::get('/huesped/troncal/{id_troncal}', 'TroncalController@getTroncal');
Route::post('/huesped/crear-troncal/{id_huesped}', 'TroncalController@storeTroncal');
Route::put('/huesped/actualizar-troncal/{id_troncal}', 'TroncalController@updateTroncal');
Route::delete('/huesped/eliminar-troncal/{id_troncal}', 'TroncalController@deleteTroncal');

// Seccion usuarios
Route::get('/huesped/listar-usuarios/{id_huesped}', 'UsuarioController@getUsuarios');
Route::get('/listar-usuarios-admin', 'UsuarioController@getUsuariosDyalogo');
Route::post('/huesped/asignar-usuario/{id_huesped}', 'UsuarioController@asignarUsuario');
Route::post('/huesped/crear-usuario/{id_huesped}', 'UsuarioController@storeUsuario');
Route::delete('/huesped/eliminar-usuario/{id_usuario}', 'UsuarioController@desvincularUsuario');
