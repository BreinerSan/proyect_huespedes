<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function encriptaPassword($pass){

        $strCad =   "".chr(33).chr(41).chr(90).chr(94).chr(77).chr(33).chr(65).chr(183)."'".chr(83).chr(68).chr(33).chr(64).chr(41).chr(35).chr(40).chr(36).chr(36).chr(35).chr(64).chr(35).chr(41).chr(95).chr(33).chr(64).chr(68).chr(70).chr(65).chr(36)."CZ".chr(35).chr(60)."AJDA".chr(62).chr(60)."ASD".chr(33).chr(64).chr(35)."M".chr(35)."N".chr(36).chr(37)."N".chr(94)."M".chr(38)."N".chr(42)."K".chr(40)."s".chr(91) .chr(92).chr(124).chr(93).chr(91).chr(47).chr(46).chr(96).chr(45).chr(43).chr(61).chr(33)."2M2xz".chr(94)."a12_%#@&\|\/".chr(46)."`'[]=-{}-1#".chr(36)."f%A%__#A!_CA?()+_!@#".chr(36)."";

        $clave = "";
        $pass2 = "";
        $CAR = "";
        $Codigo = "";
        $strLen = "";

        
       // echo $strCad;

        if (is_null($pass) || $pass == ""){
            $pass = $strCad;

        }else{
            if(strlen($pass) > 126){
                echo "La palabra es muy larga para encriptar";   

            }

            if(strlen($pass) < 10){
                $strLen = "00".strlen($pass);
                
            }

            if(strlen($pass) > 10 && strlen($pass) < 100){
                $strLen = "0".strlen($pass);
            }
                                  
            if(strlen($pass) >= 100){
                $strLen = strlen($pass);
            }
            
            
            $pass = $pass . substr($strCad, strlen($pass), strlen($strCad)) . $strLen;
       
        }


        $clave = chr(33).chr(123).chr(37).chr(125).chr(252).chr(40).chr(38).chr(41).chr(64).chr(47).chr(42).chr(64).chr(96).chr(94).chr(64).chr(35).chr(36).chr(33).chr(95).chr(91).chr(93);
        $pass2 = "";

       
        $jose = '';
        
        for($i = 0; $i < strlen($pass); $i++){

            $CAR =  substr($pass, $i, 1);

            $Codigo = substr($clave, (($i - 1) % strLen($clave)) + 1, 1);
           
            if($Codigo == ''){
                //echo "<br> ".'aja';
                $Codigo = substr($clave, 0, 1);
            }
            //echo "<br> ITERACCION ".$i." CAR => ".$CAR." , Codigo => ".$this->vaores($Codigo);
            //echo "<br> Este es el que va => ".  $this->right("0". dechex(ord($Codigo) ^ ord($CAR)), 2);
            $pass2 = $pass2 . $this->right("0". dechex(ord($Codigo) ^ ord($CAR)), 2);

        } 
        return strtoupper($pass2);
     
    } 

	public function right($strStr_p, $intLength_p)
	{
		return substr($strStr_p, -$intLength_p);
    }
    
    public function crearPassword(){

        $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";

        $password = "";
        //Reconstruimos la contraseña segun la longitud que se quiera
        for ($i = 0; $i < 8; $i++) {
            //obtenemos un caracter aleatorio escogido de la cadena de caracteres
            $password .= substr($str, rand(0, 62), 1);
        }
    
        //Mostramos la contraseña generada
        return $password;
    }
}
