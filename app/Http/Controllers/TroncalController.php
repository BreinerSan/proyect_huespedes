<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use App\Models\Troncal;
use App\Models\TroncalConfiguracion;

class TroncalController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    
    /**
     * Obtine todas las troncales de un huesped en especifico
     * @param int $id_huesped
     * @return \Illuminate\Http\Response
     */
    public function getTroncales($id_huesped){

        $troncales = Troncal::where('id_proyecto', $id_huesped)->get();

        $data = [
            'code' => 200,
            'status' => 'success',
            'troncales' => $troncales
        ];

        return response()->json($data, $data['code']);
    }

    /**
     * Obtinen una troncal en especifico
     * @param int $id_troncal
     * @return \Illuminate\Http\Response
     */
    public function getTroncal($id_troncal){

        $troncal = Troncal::where('id', $id_troncal)->with('configuraciones', 'configuraciones.propiedad')->first();

        $data = [
            'code' => 200,
            'status' => 'success',
            'troncal' => $troncal
        ];

        return response()->json($data, $data['code']);
    }

    /**
     * Esta funcion se encarga de crear una troncal para un huesped en especifico
     * @param \Illuminate\Http\Request  $request
     * @param int $id_huesped
     * @return \Illuminate\Http\Response
     */
    public function storeTroncal(Request $request, $id_huesped){
        // Validar los datos
        $validate = \Validator::make($request->all(), [
            'troncal_nombre_usuario'        => 'required|max:200',
            'troncal_codigo_cuenta'         => 'max:200',
            'troncal_tipo'                  => 'required|max:200',
            'troncal_direccion_servidor'    => 'required|max:200',
            'troncal_usuario_defecto'       => 'required|max:200',
            'troncal_contrasena'            => 'required|max:200',
            'troncal_fuente'                => 'max:200',
            'troncal_compensar_rfc2833'     => 'required|max:200',
            'troncal_limite_llamadas'       => 'required|max:200',
            'troncal_contexto'              => 'required|max:200',
            'troncal_habilitar_puente_rtp'  => 'required|max:200',
            'troncal_autenticacion'         => 'required|max:200',
            'troncal_nat'                   => 'required|max:200',
            'troncal_permitir_verificacion' => 'required|max:200',
        ]);

        if($validate->fails()){
            $data = [
                'code' => 422,
                'status' => 'error',
                'message' => 'Los datos no son validos',
                'errors' => $validate->errors()
            ];
        }else{
            
            $con1 = DB::connection('general');
            $con2 = DB::connection('telefonia');

            $con1->beginTransaction();
            $con2->beginTransaction();

            try {

                $troncal = new Troncal();

                $troncal->nombre_interno = 'trx_'.$id_huesped.'_troncal';
                $troncal->nombre_usuario = $request->troncal_nombre_usuario;
                $troncal->tipo = 'sip';
                $troncal->borrado = 0;
                $troncal->id_proyecto = $id_huesped;
                $troncal->codigo_cuenta = $request->troncal_codigo_cuenta;
                $troncal->usar_codigo_antepuesto = ($request->troncal_usar_codigo_antepuesto) ? 1 : 0;
                $troncal->save();
                
                $troncal->nombre_interno = 'trx_'.$id_huesped.'_'.$troncal->id;
                $troncal->save();

                // En esta seccion se registrara las propiedades de la troncal en dyalogo_telefonia.dy_configuracion_troncales
                $configuracionTroncal = TroncalConfiguracion::insert([
                    ['id_troncal'=> $troncal->id, 'id_propiedad'=> 51, 'valor'=> $request->troncal_tipo, 'orden'=> 1],
                    ['id_troncal'=> $troncal->id, 'id_propiedad'=> 43, 'valor'=> $request->troncal_direccion_servidor, 'orden'=> 2],
                    ['id_troncal'=> $troncal->id, 'id_propiedad'=> 39, 'valor'=> $request->troncal_usuario_defecto, 'orden'=> 3],
                    ['id_troncal'=> $troncal->id, 'id_propiedad'=> 42, 'valor'=> $request->troncal_fuente, 'orden'=> 4],
                    ['id_troncal'=> $troncal->id, 'id_propiedad'=> 49, 'valor'=> $request->troncal_contrasena, 'orden'=> 5],
                    ['id_troncal'=> $troncal->id, 'id_propiedad'=> 48, 'valor'=> $request->troncal_compensar_rfc2833, 'orden'=> 6],
                    ['id_troncal'=> $troncal->id, 'id_propiedad'=> 35, 'valor'=> $request->troncal_limite_llamadas, 'orden'=> 7],
                    ['id_troncal'=> $troncal->id, 'id_propiedad'=> 38, 'valor'=> $request->troncal_contexto, 'orden'=> 8],
                    ['id_troncal'=> $troncal->id, 'id_propiedad'=> 37, 'valor'=> $request->troncal_habilitar_puente_rtp, 'orden'=> 9],
                    ['id_troncal'=> $troncal->id, 'id_propiedad'=> 52, 'valor'=> $request->troncal_autenticacion, 'orden'=> 10],
                    ['id_troncal'=> $troncal->id, 'id_propiedad'=> 44, 'valor'=> $request->troncal_nat, 'orden'=> 11],
                    ['id_troncal'=> $troncal->id, 'id_propiedad'=> 47, 'valor'=> $request->troncal_permitir_verificacion, 'orden'=> 12],
                    ['id_troncal'=> $troncal->id, 'id_propiedad'=> 68, 'valor'=> ($request->troncal_codec_u) ? 1 : 0, 'orden'=> 13],
                    ['id_troncal'=> $troncal->id, 'id_propiedad'=> 69, 'valor'=> ($request->troncal_codec_a) ? 1 : 0, 'orden'=> 14],
                    ['id_troncal'=> $troncal->id, 'id_propiedad'=> 70, 'valor'=> ($request->troncal_g729) ? 1 : 0, 'orden'=> 15]
                ]);

                $con1->commit();
                $con2->commit();

                // Llamade del web service de guardado y serializado
                $WSApi = new \WSRest();
                $dataApi = $WSApi->troncalPersistir($troncal->id);
                $json = json_decode($dataApi);

                $data = [
                    'code' => 200,
                    'status' => 'success',
                    'message' => 'Se ha creado la troncal',
                    'troncal' => $troncal, 
                    'json' => $json
                ];
                
            } catch (\Throwable $th) {
                //throw $th;
                $con1->rollBack();
                $con2->rollBack();

                return $e;

                $data = [
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'Se ha presentado un error al guardar los datos',
                ];
            }
            
        }

        return response()->json($data, $data['code']);

    }

    /**
     * Actualiza los datos de una troncal
     * @param \Illuminate\Http\Request  $request
     * @param int $id_huesped
     * @return \Illuminate\Http\Response
     */
    public function updateTroncal(Request $request, $id_troncal){
        // Validar los datos
        $validate = \Validator::make($request->all(), [
            'troncal_nombre_usuario'        => 'required|max:200',
            'troncal_codigo_cuenta'         => 'max:200',
            'troncal_tipo'                  => 'required|max:200',
            'troncal_direccion_servidor'    => 'required|max:200',
            'troncal_usuario_defecto'       => 'required|max:200',
            'troncal_contrasena'            => 'required|max:200',
            'troncal_fuente'                => 'max:200',
            'troncal_compensar_rfc2833'     => 'required|max:200',
            'troncal_limite_llamadas'       => 'required|max:200',
            'troncal_contexto'              => 'required|max:200',
            'troncal_habilitar_puente_rtp'  => 'required|max:200',
            'troncal_autenticacion'         => 'required|max:200',
            'troncal_nat'                   => 'required|max:200',
            'troncal_permitir_verificacion' => 'required|max:200',
        ]);

        if($validate->fails()){
            $data = [
                'code' => 422,
                'status' => 'error',
                'message' => 'Los datos no son validos',
                'errors' => $validate->errors()
            ];
        }else{

            $con1 = DB::connection('general');
            $con2 = DB::connection('telefonia');

            $con1->beginTransaction();
            $con2->beginTransaction();

            try {
            
                $troncal = Troncal::find($id_troncal);

                $troncal->nombre_usuario = $request->troncal_nombre_usuario;
                $troncal->codigo_cuenta = $request->troncal_codigo_cuenta;
                $troncal->usar_codigo_antepuesto = ($request->troncal_usar_codigo_antepuesto) ? 1 : 0;
                $troncal->save();

                // Metodo para actualizar los campos de propiedades de la troncal
                TroncalConfiguracion::where('id_troncal', $id_troncal)->where('id_propiedad', 51)->update(['valor' => $request->troncal_tipo]);
                TroncalConfiguracion::where('id_troncal', $id_troncal)->where('id_propiedad', 43)->update(['valor' => $request->troncal_direccion_servidor]);
                TroncalConfiguracion::where('id_troncal', $id_troncal)->where('id_propiedad', 39)->update(['valor' => $request->troncal_usuario_defecto]);
                TroncalConfiguracion::where('id_troncal', $id_troncal)->where('id_propiedad', 42)->update(['valor' => $request->troncal_fuente]);
                TroncalConfiguracion::where('id_troncal', $id_troncal)->where('id_propiedad', 49)->update(['valor' => $request->troncal_contrasena]);
                TroncalConfiguracion::where('id_troncal', $id_troncal)->where('id_propiedad', 48)->update(['valor' => $request->troncal_compensar_rfc2833]);
                TroncalConfiguracion::where('id_troncal', $id_troncal)->where('id_propiedad', 35)->update(['valor' => $request->troncal_limite_llamadas]);
                TroncalConfiguracion::where('id_troncal', $id_troncal)->where('id_propiedad', 38)->update(['valor' => $request->troncal_contexto]);
                TroncalConfiguracion::where('id_troncal', $id_troncal)->where('id_propiedad', 37)->update(['valor' => $request->troncal_habilitar_puente_rtp]);
                TroncalConfiguracion::where('id_troncal', $id_troncal)->where('id_propiedad', 52)->update(['valor' => $request->troncal_autenticacion]);
                TroncalConfiguracion::where('id_troncal', $id_troncal)->where('id_propiedad', 44)->update(['valor' => $request->troncal_nat]);
                TroncalConfiguracion::where('id_troncal', $id_troncal)->where('id_propiedad', 47)->update(['valor' => $request->troncal_permitir_verificacion]);
                TroncalConfiguracion::where('id_troncal', $id_troncal)->where('id_propiedad', 68)->update(['valor' => ($request->troncal_codec_u) ? 1 : 0]);
                TroncalConfiguracion::where('id_troncal', $id_troncal)->where('id_propiedad', 69)->update(['valor' => ($request->troncal_codec_a) ? 1 : 0]);
                TroncalConfiguracion::where('id_troncal', $id_troncal)->where('id_propiedad', 70)->update(['valor' => ($request->troncal_g729) ? 1 : 0]);

                $con1->commit();
                $con2->commit();

                $data = [
                    'code' => 200,
                    'status' => 'success',
                    'message' => 'Se ha actualizado el registro exitosamente',
                    'troncal' => $troncal
                ];

            } catch (\Throwable $th) {
                //throw $th;
                $con1->rollBack();
                $con2->rollBack();

                return $e;

                $data = [
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'Se ha presentado un error al guardar los datos',
                ];
            }

        }
        return response()->json($data, $data['code']);
    }

    /**
     * Elimina la troncal especifica
     * @param int $id_troncal
     * @return \Illuminate\Http\Response
     */
    public function deleteTroncal($id_troncal){
        
        // Llamade del web service de guardado y serializado
        $WSApi = new \WSRest();
        $dataApi = $WSApi->troncalBorrar($id_troncal);
        $json = json_decode($dataApi);

        TroncalConfiguracion::where('id_troncal', $id_troncal)->delete();
        
        Troncal::find($id_troncal)->delete();
        
        $data = [
            'code' => 200,
            'status' => 'success',
            'message' => 'Su registro ha sido eliminado', 
            'json' => $json
        ];

        return response()->json($data, $data['code']);
    }

    /**
     * Esta funcion se encarga de obtener los estados de las troncales de un huesped en especifico
     * @param int $id_huesped
     * @return \Illuminate\Http\Response
     */
    public function getEstadoTroncales($id_huesped){
        $troncales = Troncal::where('id_proyecto', $id_huesped)->get();

        $troncalEstado = [];

        foreach ($troncales as $troncal) {

            $WSApi = new \WSRest();
            $dataApi = $WSApi->troncalEstado($troncal->id);
            $json = json_decode($dataApi);

            if(!$json){
                $troncalEstado[$troncal->id] = 'fallo';
            }else{
                $troncalEstado[$troncal->id] = $json->objSerializar_t;
            }
        }

        $data = [
            'code' => 200,
            'status' => 'success',
            'troncalEstado' => $troncalEstado
        ];

        return response()->json($data, $data['code']);
    }
}
