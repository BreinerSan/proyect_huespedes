<div class="modal fade" id="modal-test-cuentaCorreo" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Prueba cuenta de correo</h4>
            </div>
            <div class="modal-body">
                <form action="" id="form-test-cuenta-correo" method="POST">
                    @csrf
                    <input type="hidden" id="id_cuentaCorreo" name="id_cuentaCorreo">
                    <input type="hidden" id="cuentaCorreoUsuario" name="cuentaCorreoUsuario">
                    <input type="hidden" id="id_huesped" name="id_huesped">
                    <h4 class="cuenta-correo-nombre"></h4>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="destinatario">Destinatario</label>
                            <input type="text" name="destinatario" id="destinatario" class="form-control" placeholder="Correo del destinatario">
                        </div>
                    </div>
    
                </form>

                <div class="row">
                    <div class="col-md-12">
                        <textarea name="consoleLogCorreo" id="consoleLogCorreo" cols="30" rows="10" class="form-control text-left" readonly style="display:none">
                        </textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="text-success pull-left estado-test-mail-salida" style="margin-top:0px;display:none"></h3><br>
                    </div>
                    <div class="col-md-12">
                        <h3 class="text-success pull-left estado-test-mail-entrada" style="margin-top:0px;display:none"></h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <button type="button" class="btn btn-primary" id="btn-test-cuenta">Iniciar prueba</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    </div>       
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <h4 style="text-align: center;">Despues de iniciar la prueba espere a que el sistema termine de cargar y devuelva una respuesta</h4>
                    </div>
                </div>
                
            </div>
            
        </div>
        
    </div>
    
</div>
        