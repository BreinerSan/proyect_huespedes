<div class="panel box box-primary e-collapse">
    <div class="box-header with-border">
        <h4 class="box-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseCuentasCorreo">
            CUENTAS DE CORREO
        </a>
        </h4>
    </div>
    <div id="collapseCuentasCorreo" class="panel-collapse collapse">
        <div class="box-body">
            <h4 class="text-aqua">Lista de cuentas de correo del huésped</h4>
            <table class="table">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Proveedor</th>
                        <th>Dirección de correo electrónico</th>
                        <th>Estado</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
            <br>
            <button type="button" id="btn-crear-cuenta-correo" class="btn btn-success btn-sm pull-right"><i class="fa fa-plus"></i> Agregar</button>
        </div>
    </div>
</div>
@include('huesped.modals.cuentaCorreoModal')
@include('huesped.modals.testCuentaCorreoModal')
