<div class="panel box box-primary e-collapse">
    <div class="box-header with-border">
        <h4 class="box-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTroncales">
            TRONCALES
        </a>
        </h4>
    </div>
    <div id="collapseTroncales" class="panel-collapse collapse">
        <div class="box-body">
            <h4 class="text-aqua">Troncales</h4>
            <form action="" id="form-troncales">
                @csrf   
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Nombre de usuario</th>
                            <th>Codigo de cuenta</th>
                            <th>Estado</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                       
                    </tbody>
                </table>
            </form>
            <br>
            <button type="button" id="btn-agregar-troncal" class="btn btn-success btn-sm pull-right"><i class="fa fa-plus"></i> Agregar</button>
        </div>
    </div>
</div>

@include('huesped.modals.troncal')  
