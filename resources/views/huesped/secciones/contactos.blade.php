<div class="panel box box-primary">
    <div class="box-header with-border contacto-header">
        <h4 class="box-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
            CONTACTOS
        </a>
        </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse">
        <div class="box-body">
            <div id="panel-contacto">
                @include('huesped.forms.contacto')
            </div>
            <button type="button" id="add-contacto" title="Crear nuevo contacto" class="btn btn-success btn-sm pull-right"><i class="fa fa-plus"></i> Agregar contacto</button>
        </div>
    </div>
</div>
