<div class="panel box box-primary e-collapse">
    <div class="box-header with-border contacto-header">
        <h4 class="box-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseProveedoresSms">
            PROVEEDORES DE SMS
        </a>
        </h4>
    </div>
    <div id="collapseProveedoresSms" class="panel-collapse collapse">
        <div class="box-body">
            <h4 class="text-aqua">Lista de proveedores de SMS</h4>
            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Proveedor</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                
                </tbody>
            </table>
            <br>
            <button type="button" id="btn-crear-proveedor-sms" class="btn btn-success btn-sm pull-right"><i class="fa fa-plus"></i> Agregar</button>
        </div>
    </div>
</div>
@include('huesped.modals.proveedorSmsModal')           
@include('huesped.modals.pruebaProveedorSmsModal')
