<div class="panel box box-primary e-collapse">
    <div class="box-header with-border contacto-header">
        <h4 class="box-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapsePausas">
            PAUSAS
        </a>
        </h4>
    </div>
    <div id="collapsePausas" class="panel-collapse collapse">
        <div class="box-body">
            <h4 class="text-aqua">Lista de pausas</h4>
            <form action="" id="form-pausas">
                @csrf   
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th style="width:120px;">Clasificación</th>
                            <th style="width:130px;"> Tipo de programación</th>
                            <td style="width:100px;">Hora inicial por defecto</td>
                            <td style="width:100px;">Hora final por defecto</td>
                            <td style="width:80px;">Duración máxima al día</td>
                            <td style="width:80px;">Cantidad máxima al día</td>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    
                    </tbody>
                </table>
            </form>
            <br>
            <button type="button" id="btn-agregar-pausa" class="btn btn-success btn-sm pull-right"><i class="fa fa-plus"></i> Agregar</button>
            <button type="button" id="btn-guardar-cambios-pausas" class="btn btn-primary btn-sm pull-right" style="margin-right:5px;"><i class="fa fa-save"></i> Guardar cambios</button>
        </div>
    </div>
</div>
